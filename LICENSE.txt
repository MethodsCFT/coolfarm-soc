MIT Licence
 
Copyright © 2023 Cool Farm Alliance Community Interest Company 
 
The Cool Farm Alliance Community Interest Company, a community interest company registered in England and Wales under company number 09075620 (“the CFA”) hereby grants, free of charge, to any person obtaining a copy of these models and associated documentation files (the "Software"), a non-exclusive, worldwide, revocable, royalty-free, fully-paid, non-sellable and non-sublicensable licence to use, copy, modify, merge, publish and distribute copies of the Software, and to permit persons to whom the Software is furnished to do so (the “Licence”). The Licence is conditional on the following conditions being met:
 
1. The above copyright notice and this licence must be included in all copies or substantial portions of the Software.
2. The Software must be used solely for lawful purpose and in a manner that does not infringe upon the rights, including without limitation intellectual property rights, of any third party.
3. Property attribution must be given to the CFA as the copyright owner of the Software. Any distribution or publication of the Software, whether modified or unmodified, shall include an acknowledgment of the CFA's ownership.
 
The Licence shall be immediately revoked in the event of any breach of the terms of this Licence. 

THE SOFTWARE IS PROVIDED "AS IS," WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF THE INTELLECTUAL PROPERTY RIGHTS OF THIRD PARTIES. THE CFA DOES NOT WARRANT THAT THE SOFTWARE IS FREE FROM ANY VIRUS OR VULNERABILITY. 

IN NO EVENT SHALL THE CFA BE LIABLE FOR ANY CLAIM, LOSS, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT, OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE SOFTWARE, USE OF THE SOFTWARE, EXERCISE OF THE LICENCE GRANTED HEREIN, OR ANY OTHER DEALINGS IN THE SOFTWARE.
 
DISCLAIMER: THE CFA AND ITS CONTRIBUTORS SHALL NOT BE LIABLE FOR ANY CLAIM, LOSS, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT, OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE SOFTWARE, USE OF THE SOFTWARE, EXERCISE OF THE LICENCE GRANTED HEREIN, OR ANY OTHER DEALINGS IN THE SOFTWARE. YOU USE THIS SOFTWARE SOLELY AT YOUR OWN RISK.
