**Original file:** Vol4_Ch5_Tier2_Steady_State_Method-Spreadsheet.xlsx

**Source:** https://www.ipcc-nggip.iges.or.jp/public/2019rf/vol4.html

*Extracts have been made from the original .xlsx to allow IPCC data to be more readily used for testing the model implementation:*

* ipcc_climate_data.csv: Extract from original file, Climate Data tab, A6:F817. Columns are Year, Month, Year-month, Average temperature (C), Total rainfall (mm), Total evapotranspiration (mm). __NOTE: This file is located in the cfasoc module so that it is included in the distribution.__
* carbon_inputs.csv: Extract from original file, Carbon inputs tab, D4:F62. Columns are Year, WheatSDM (Annual Shoot Dry Matter, kg/ha), CI (Annual Carbon Input, Mg C/ha).
* model_parameters.csv: Extract from original file, Model Parameters tab, A3:E37. Columns as labelled.
* time_sequence.csv: Extract from original file, Time sequence tab, A3:Y62. Columns as labelled.
* ti_wi.csv: Extract from original file, Climate Data tab, A1:C69. Columns as labelled.
