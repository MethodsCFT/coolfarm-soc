import numpy as np
from numpy import array as a
import sys
from pathlib import Path
ROOT_DIR = str(Path(__file__).parent.parent.parent)
if ROOT_DIR + "/src" not in sys.path: sys.path.append(ROOT_DIR + "/src")
import cfasoc.ipcc.model as md
from cfasoc.climate import DemoClimateData

test_data_path = ROOT_DIR + "/test/ipcc/test_data"
mp = np.genfromtxt(test_data_path + "/model_parameters.csv", delimiter=',', dtype = str)
ci = np.genfromtxt(test_data_path + "/carbon_inputs.csv", delimiter=',', dtype = str)
ts = np.genfromtxt(test_data_path + "/time_sequence.csv", delimiter=',', dtype = str)
cd = np.genfromtxt(test_data_path + "/ti_wi.csv", delimiter=',', dtype = str)
demo_climate_data = DemoClimateData()

def test_run() -> None:
    t_fac = ts[13:, 3].astype(float)
    w_fac = ts[13:, 4].astype(float)
    c_input = ci[1:,2].astype(float)
    timestamps = None
    active_init = a([0.344936])
    passive_init = a([58.801184])
    slow_init = a([3.604169])
    run_in_period = 5
    sand = a(mp[32, 2]).astype(float)
    lc = ts[13:, 7].astype(float)
    nc = ts[13:, 8].astype(float)
    till_fac = a(mp[1, 2]).astype(float)
    f_1 = a(mp[12, 2]).astype(float)
    f_2 = a(mp[13, 2]).astype(float)
    f_3 = a(mp[17, 2]).astype(float)
    f_5 = a(mp[21, 2]).astype(float)
    f_6 = a(mp[22, 2]).astype(float)
    f_7 = a(mp[23, 2]).astype(float)
    f_8 = a(mp[24, 2]).astype(float)
    k_fac_a = a(mp[7, 2]).astype(float)
    k_fac_s = a(mp[10, 2]).astype(float)
    k_fac_p = a(mp[11, 2]).astype(float)
    test_run = md.run(
        t_fac=t_fac,
        w_fac=w_fac,
        c_input=c_input[11:],
        timestamps=timestamps,
        active_init=active_init,
        passive_init=passive_init,
        slow_init=slow_init,
        run_in_period=run_in_period,
        sand=sand,
        lc=lc,
        nc=nc,
        till_fac=till_fac,
        f_1=f_1,
        f_2=f_2,
        f_3=f_3,
        f_5=f_5,
        f_6=f_6,
        f_7=f_7,
        f_8=f_8,
        k_fac_a=k_fac_a,
        k_fac_s=k_fac_s,
        k_fac_p=k_fac_p
    )
    test_soc = test_run.soc
    ipcc_soc = ts[13:, 23].astype(float)
    np.testing.assert_array_almost_equal(test_soc, ipcc_soc, decimal=1)
