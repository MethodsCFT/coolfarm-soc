from numpy import array as a
import numpy as np

from pathlib import Path
import sys
ROOT_DIR = str(Path(__file__).parent.parent.parent)
if ROOT_DIR + "/src" not in sys.path: sys.path.append(ROOT_DIR + "/src")

import cfasoc.ipcc.equations as eq
from cfasoc.climate import DemoClimateData

test_data_path = ROOT_DIR + "/test/ipcc/test_data"
mp = np.genfromtxt(test_data_path + "/model_parameters.csv", delimiter=',', dtype = str)
ci = np.genfromtxt(test_data_path + "/carbon_inputs.csv", delimiter=',', dtype = str)
ts = np.genfromtxt(test_data_path + "/time_sequence.csv", delimiter=',', dtype = str)
cd = np.genfromtxt(test_data_path + "/ti_wi.csv", delimiter=',', dtype = str)
demo_climate_data = DemoClimateData()

def test_delta_c_mineral_new() -> None:
    np.testing.assert_array_almost_equal(eq.delta_c_mineral(f_soc_i=a([1]), a_i = a([1])), a([1]), decimal = 3)

def test_delta_c_mineral() -> None:
    np.testing.assert_array_almost_equal(eq.delta_c_mineral(f_soc_i=a([1]), a_i = a([1])), a([1]), decimal = 3)

def test_f_soc_i() -> None:
    np.testing.assert_array_almost_equal(eq.f_soc_i(soc_y_i=a([2]), soc_y_1_i=a([1])), a([1]), decimal = 3)

def test_soc_y_i() -> None:
    np.testing.assert_array_almost_equal(eq.soc_y_i(active_y_i=a([1]), slow_y_i=a([1]), passive_y_i=a([1])), a([3]), decimal = 3)

def test_active_y() -> None:
    ACTIVEy = ts[13:, 13].astype(float)
    ACTIVEydot = ts[13:, 12].astype(float)
    kA = ts[13:, 11].astype(float)
    np.testing.assert_array_almost_equal(eq.active_y(active_y_1=ACTIVEy[0:-1], active_y_dot=ACTIVEydot[1:], k_a=kA[1:]), ACTIVEy[1:], decimal=3)

def test_active_y_dot() -> None:
    alpha = ts[13:, 10].astype(float)
    kA = ts[13:, 11].astype(float)
    ACTIVEydot = ts[13:, 12].astype(float)
    np.testing.assert_array_almost_equal(eq.active_y_dot(alpha=alpha[1:], k_a=kA[1:]), ACTIVEydot[1:], decimal = 3)

def test_k_a() -> None:
    kfaca = a(mp[7, 2]).astype(float)
    tfac = ts[13:, 3].astype(float)
    wfac = ts[13:, 4].astype(float)
    sand = a(mp[32, 2]).astype(float)
    tillfac_ft = a(mp[1, 2]).astype(float)
    kA = ts[13:, 11].astype(float)
    np.testing.assert_array_almost_equal(eq.k_a(k_fac_a=kfaca, t_fac=tfac[1:], w_fac=wfac[1:], sand=sand, till_fac=tillfac_ft), kA[1:], decimal=3)

def test_slow_y() -> None:
    SLOWy = ts[13:, 17].astype(float)
    SLOWydot = ts[13:, 16].astype(float)
    kS = ts[13:, 15].astype(float)
    np.testing.assert_array_almost_equal(eq.slow_y(slow_y_1=SLOWy[0:-1], slow_y_dot=SLOWydot[1:], k_s=kS[1:]), SLOWy[1:], decimal=3)

def test_slow_y_dot() -> None:
    ci_CI = ci[1:,2].astype(float)
    LC = a(mp[33, 2].astype(float))
    f3 = a(mp[17, 2]).astype(float)
    ACTIVEydot = ts[13:, 12].astype(float)
    kA = ts[13:, 11].astype(float)
    f4 = a(mp[20, 2]).astype(float)
    kS = ts[13:, 15].astype(float)
    SLOWydot = ts[13:, 16].astype(float)
    np.testing.assert_array_almost_equal(eq.slow_y_dot(c_input=ci_CI[12:], lc=LC, f_3=f3, active_y_dot=ACTIVEydot[1:], k_a = kA[1:], f_4=f4, k_s=kS[1:]), SLOWydot[1:], decimal = 3)

def test_k_s() -> None:
    kfacs = a(mp[10, 2]).astype(float)
    tfac = ts[13:, 3].astype(float)
    wfac = ts[13:, 4].astype(float)
    tillfac_ft = a(mp[1, 2]).astype(float)
    kS = ts[13:, 15].astype(float)
    np.testing.assert_array_almost_equal(eq.k_s(k_fac_s=kfacs, t_fac=tfac[1:], w_fac=wfac[1:], till_fac=tillfac_ft), kS[1:], decimal = 3)

def test_f_4() -> None:
    f5 = a(mp[21, 2]).astype(float)
    f4 = a(mp[20, 2]).astype(float)
    sand = a(mp[32, 2]).astype(float)
    np.testing.assert_array_almost_equal(eq.f_4(f_5=f5, sand=sand), f4, decimal=3)

def test_passive_y() -> None:
    PASSIVEy = ts[13:, 21].astype(float)
    PASSIVEydot = ts[13:, 20].astype(float)
    kP = ts[13:, 19].astype(float)
    np.testing.assert_array_almost_equal(eq.passive_y(passive_y_1=PASSIVEy[0:-1], passive_y_dot=PASSIVEydot[1:], k_p=kP[1:]), PASSIVEy[1:], decimal=3)

def test_passive_y_dot() -> None:
    ACTIVEydot = ts[13:, 12].astype(float)
    kA = ts[13:, 11].astype(float)
    f5 = a(mp[21, 2]).astype(float)
    SLOWydot = ts[13:, 16].astype(float)
    kS = ts[13:, 15].astype(float)
    f6 = a(mp[22, 2]).astype(float)
    kP = ts[13:, 19].astype(float)
    PASSIVEydot = ts[13:, 20].astype(float)
    np.testing.assert_array_almost_equal(eq.passive_y_dot(active_y_dot=ACTIVEydot[1:], k_a=kA[1:], f_5=f5, slow_y_dot=SLOWydot[1:], k_s=kS[1:], f_6=f6, k_p=kP[1:]), PASSIVEydot[1:], decimal=1)

def test_k_p() -> None:
    kfacp = a(mp[11, 2]).astype(float)
    tfac = ts[13:, 3].astype(float)
    wfac = ts[13:, 4].astype(float)
    kP = ts[13:, 19].astype(float)
    np.testing.assert_array_almost_equal(eq.k_p(k_fac_p=kfacp, t_fac=tfac[1:], w_fac=wfac[1:]), kP[1:], decimal=3)

def test_t_fac() -> None:
    Ti = cd[1:, 2].astype(float)
    tfac = ts[13:, 3].astype(float)
    np.testing.assert_array_almost_equal(eq.t_fac(t_i = Ti[240:804]), tfac, decimal=2)

def test_t_i() -> None:
    tmax = a(mp[27, 2]).astype(float)
    temp_i = demo_climate_data.temp
    topt = a(mp[28, 2]).astype(float)
    ti_array = cd[1:,2].astype(float)
    ti = ti_array.reshape(len(ti_array), 1)
    np.testing.assert_array_almost_equal(eq.t_i(t_max = tmax, temp_i=temp_i, t_opt=topt), ti[:804], decimal=2)

def test_w_fac() -> None:
    Wi = cd[1:, 4].astype(float)
    wfac = ts[13:, 4].astype(float)
    np.testing.assert_array_almost_equal(eq.w_fac(w_i=Wi[240:804]), wfac, decimal=3)

def test_w_i() -> None:
    wfacpar2 = a(mp[5, 2]).astype(float)
    w_s = wfacpar2
    mappeti = cd[1:, 3].astype(float)
    Wi = cd[1:, 4].astype(float)
    np.testing.assert_array_almost_equal(eq.w_i(w_s = w_s, mappet_i = mappeti), Wi, decimal=3)

def test_mappet_i() -> None:
    precip_i = demo_climate_data.rain
    pet_i = demo_climate_data.pet
    mappet_i_array = cd[1:, 3].astype(float)
    mappet_i = mappet_i_array.reshape(len(mappet_i_array), 1)
    np.testing.assert_array_almost_equal(eq.mappet_i(precip_i=precip_i, pet_i=pet_i), mappet_i[:-8], decimal=3)

def test_alpha() -> None:
    Beta = ts[13:, 9].astype(float)
    f1 = a(mp[12, 2]).astype(float)
    ci_CI = ci[1:,2].astype(float)
    ts_LC = ts[13:, 7].astype(float)
    f2 = a(mp[13, 2]).astype(float)
    f3 = a(mp[17, 2]).astype(float)
    f7 = a(mp[23, 2]).astype(float)
    f6 = a(mp[22, 2]).astype(float)
    f8 = a(mp[24, 2]).astype(float)
    f4 = a(mp[20, 2]).astype(float)
    f5 = a(mp[21, 2]).astype(float)
    alpha = ts[13:, 10].astype(float)
    np.testing.assert_array_almost_equal(eq.alpha(beta=Beta, f_1=f1, c_input=ci_CI[11:], lc=ts_LC, f_2=f2, f_3=f3, f_7=f7, f_6=f6, f_8=f8, f_4=f4, f_5=f5), alpha, decimal=3)

def test_beta() -> None:
    ci_CI = ci[1:,2].astype(float)
    ts_LC = ts[13:, 7].astype(float)
    ts_NC = ts[13:, 8].astype(float)
    Beta = ts[13:, 9].astype(float)
    np.testing.assert_array_almost_equal(eq.beta(c_input=ci_CI[11:], lc=ts_LC, nc=ts_NC), Beta, decimal=3)
