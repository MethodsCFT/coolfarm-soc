from pathlib import Path
import sys
ROOT_DIR = str(Path(__file__).parent.parent.parent)
if ROOT_DIR + "/src" not in sys.path: sys.path.append(ROOT_DIR + "/src")
import numpy as np
from numpy import array as a
import cfasoc.rothc.equations as eq
import cfasoc.builders as bd


def test_rmf_a() -> None:
    """Input and expected values determined using SoilR.

    Note that there are some minor differences between values calculated using SoilR and
    this package. This is because for this equation, SoilR uses slightly different rounding
    in the underlying function (fT.RothC in SoilR) to that recommended in the RothC documentation.
    In SoilR:
    
    "47.9/(1 + exp(106/(ifelse(Temp >= -18.3, Temp, NA) + 18.3)))"
    
    , but in the RothC documentation, the R equivalent is:
    
    "47.91/(1 + exp(106.06/(ifelse(Temp >= -18.27, Temp, NA) + 18.27)))".

    This function (rmf_a) uses the method described in the RothC documentation.
    """
    temp = a([-0.4, 0.3, 4.2, 8.3, 13.0, 15.9, 18.0, 17.5, 13.4, 8.7, 3.9,  0.6])
    rmf_a_expected = a([0.1280481, 0.1599085, 0.4270090, 0.8743546, 1.5671668, 2.0659933, 2.4509640, 2.3578371, 1.6332549, 0.9264990, 0.4008918, 0.1749929])
    rmf_a_calculated = eq.rmf_a(temp)
    np.testing.assert_array_almost_equal(rmf_a_expected, rmf_a_calculated, decimal=2)


def test_rmf_b_soilr() -> None:
    precip = a([49, 39, 44, 41, 61, 58, 71, 58, 51, 48, 50, 58]).reshape(12,1)
    evap = a([12, 18, 35, 58, 82, 90, 97, 84, 54, 31, 14, 10]).reshape(12,1)
    thickness = a([25])
    clay = a([48])
    vegetated = bd.repeat_single_bool(shape=(12,1),value=True)
    rmf_b_expected = a([1.0000000, 1.0000000, 1.0000000, 1.0000000, 0.7914406, 0.2000000, 0.2000000, 0.2000000, 0.2000000, 0.5791037, 1.0000000, 1.0000000]).reshape(12,1)
    
    b = eq.b_soilr(vegetated=vegetated)
    max_tsmd = eq.max_tsmd_soilr(clay=clay, thickness=thickness, B=b)
    m = eq.m_soilr(precip=precip, evap=evap)
    acc_tsmd = eq.acc_tsmd_soilr(m=m, max_tsmd=max_tsmd)
    rmf_b_calculated = eq.rmf_b_soilr(acc_tsmd, max_tsmd)

    np.testing.assert_array_almost_equal(rmf_b_expected, rmf_b_calculated)
