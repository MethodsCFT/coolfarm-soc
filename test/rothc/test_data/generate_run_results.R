# Run the SoilR package for R to generate some test results to test the Python
# implementation of RothC against.
# https://www.bgc-jena.mpg.de/TEE/basics/2015/11/19/RothC/
# 1. Using clay content, litter inputs, and climate data

require(SoilR)
require(rjson)
this.dir <- dirname(parent.frame(2)$ofile)
setwd(this.dir)

all_test_data = fromJSON(file="run_data.json")

for(i in names(all_test_data))
{
  test_data = all_test_data[[i]]
  
  Temp=data.frame(Month=1:12, Temp=test_data$Temp)
  Precip=data.frame(Month=1:12, Precip=test_data$Precip)
  Evp=data.frame(Month=1:12, Evp=test_data$Evp)
  
  soil.thick=test_data$soil.thick  #Soil thickness (organic layer topsoil), in cm
  SOC=test_data$SOC                #Soil organic carbon in Mg/ha 
  clay=test_data$clay              #Percent clay
  Cinputs=test_data$Cinputs        #Annual C inputs to soil in Mg/ha/yr
  bare=!test_data$vegetated        #Is the ground bare (TRUE) or vegetated (FALSE)?
  p_e=test_data$p_e
  fym = test_data$fym
  
  years=seq(1/12,test_data$years,by=1/12)
  
  fT=fT.RothC(Temp[,2]) #Temperature effects per month
  fW=fW.RothC(P=(Precip[,2]), E=(Evp[,2]),
              S.Thick = soil.thick, pClay = clay,
              pE = p_e, bare = bare)$b #Moisture effects per month
  rmf_c = ifelse(bare, 1, 0.6) #Rate modifying factor for soil cover
  xi.frame=data.frame(years,rep(fT*fW*rmf_c,length.out=length(years)))
  
  FallIOM=0.049*SOC^(1.139) #IOM using Falloon method
  
  Model1=RothCModel(t=years,C0=c(DPM=0, RPM=0, BIO=0, HUM=0, IOM=FallIOM),
                    In=Cinputs, clay=clay, xi=xi.frame, FYM=fym) #Loads the model
  Ct1=getC(Model1) #Calculates stocks for each pool per month
  
  matplot(years, Ct1, type="l", lty=1, col=1:5,
          xlab="Time (years)", ylab="C stocks (Mg/ha)")
  legend("topleft", c("DPM", "RPM", "BIO", "HUM", "IOM"),
         lty=1, col=1:5, bty="n")
  
  poolSize1=as.numeric(tail(Ct1,1))
  names(poolSize1)<-c("DPM", "RPM", "BIO", "HUM", "IOM")
  
  # Write results to .csv for use in tests
  colnames(Ct1) = c("DPM", "RPM", "BIO", "HUM", "IOM")
  write.csv(Ct1, paste0("run_results_", i, ".csv"))
}