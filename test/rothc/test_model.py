import numpy as np
from numpy import array as a
import json
from matplotlib import pyplot as plt
from typing import Union
from pathlib import Path
import sys
ROOT_DIR = str(Path(__file__).parent.parent.parent)
if ROOT_DIR + "/src" not in sys.path: sys.path.append(ROOT_DIR + "/src")
import cfasoc.rothc.equations as eq
import cfasoc.builders as bd
import cfasoc.rothc.model as m

def test_run(id: Union[int, None] = None, plot: bool = False) -> None:
    """This function tests that the Python implementation of RothC (this package) produces results equivalent to
    those generated using the R package SoilR when given equivalent model inputs. Run data (i.e. the model inputs
    given to both SoilR and this package) are in test/test_data/run_data.json. The SoilR script used to generate
    SoilR results is test/test_data/generate_run_results.R. This script reads run_data.json and produces N
    output datasets, run_results_1.csv, ...2.csv, and ...N.csv which correspond to the expected results given the
    N model input datasets. This function also provides the ability to run just one of the N model input
    datasets and plot results for visual confirmation. Additional model input datasets can be added to 
    test/test_data/run_data.json, however these will also need to be recalculated using test/test_data/generate_run_results.R.

    This test checks for agreement up to 0 decimal places due to minor differences in the way the actual results
    are calculated in SoilR and this package. 
    
    Args:
        id (Union[int, None], optional): Which test model inputs should be run (1, 2, or 3). Defaults to None (if None, all are run).
        plot (bool, optional): Should the test results be plotted? Defaults to False.
    """
    with open('test/rothc/test_data/run_data.json') as f:
        TEST_DATA = json.load(f)

    test_runs = TEST_DATA.keys() if id is None else [str(id)]
    for test_id in test_runs:

        # Load input data to test runs
        years = TEST_DATA[test_id]["years"]
        iters = 1
        precip_year = a(TEST_DATA[test_id]["Precip"]).reshape(12,1)
        pet_year = a([TEST_DATA[test_id]["Evp"]]).reshape(12,1)
        temp_year = a([TEST_DATA[test_id]["Temp"]]).reshape(12,1)
        vegetated_year = bd.repeat_single_bool(shape=(12,iters), value=TEST_DATA[test_id]["vegetated"])
        vegetated=bd.repeat_single_bool(shape=(years*12, iters), value=TEST_DATA[test_id]["vegetated"])
        thickness = a([TEST_DATA[test_id]["soil.thick"]])
        clay = a([TEST_DATA[test_id]["clay"]])
        c_input_annual = TEST_DATA[test_id]["Cinputs"]
        c_input_monthly = c_input_annual/12
        c_input = bd.repeat_single(shape=(years*12,1), value=c_input_monthly)
        p_e = a([TEST_DATA[test_id]["p_e"]])
        fym_monthly = TEST_DATA[test_id]["fym"]/12
        soc = a([TEST_DATA[test_id]["SOC"]])

        # Load expected results, determined using SoilR in R
        expected_res = np.genfromtxt('test/rothc/test_data/run_results_' + test_id + '.csv', delimiter=',')[1:,1:]
        expected_soc = expected_res[:,0] + expected_res[:,1] + expected_res[:,2] + expected_res[:,3] + expected_res[:,4]

        # Calculate results using the Python implementation of RothC (this package)
        rmf_a_year=m.calc_rmf_a(temperature=temp_year)
        rmf_a = bd.stack_arrays([rmf_a_year for _ in range(years)])
        rmf_b_year = m.calc_rmf_b(precip=precip_year, pet=pet_year, vegetated=vegetated_year, clay=clay, thickness=thickness, p_e=p_e)
        rmf_b = bd.stack_arrays([rmf_b_year for _ in range(years)])
        rmf_c = m.calc_rmf_c(vegetated=vegetated)
        fym = bd.repeat_single(shape=(years*12,iters), value=fym_monthly)
        iom = eq.iom(toc=soc)
        calculated_res = m.run(c_input=c_input, rmf_a=rmf_a, rmf_b=rmf_b, rmf_c=rmf_c, clay=clay, fym=fym, init_iom=iom)
        
        # Check for equality to 0 decimal places
        np.testing.assert_array_almost_equal(expected_soc.reshape(years*12,1), calculated_res.soc, decimal=0)

        # Plot for visual confirmation
        if plot:
            plt.plot(expected_soc, color="blue", linewidth=5)
            plt.plot(calculated_res.soc, color="red", linewidth=0.5)
