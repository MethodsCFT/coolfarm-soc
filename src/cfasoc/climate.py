import numpy as np
from numpy import array as a
from typing import List
import requests
from operator import itemgetter
from cfasoc.ipcc import CLIMATE_DATA


MONTHS = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]


class Climate:
    def __init__(self) -> None:
        self.years: np.ndarray
        self.months: np.ndarray
        self.temp: np.ndarray
        self.rain: np.ndarray
        self.pet: np.ndarray
        
    def _load_data(self) -> None:
        pass


class DemoClimateData(Climate):
    def __init__(self) -> None:
        self._load_data()

    def _load_data(self) -> None:
        no_year_month = np.delete(CLIMATE_DATA, 2, 1)
        _ = np.delete(no_year_month, slice(804, 812, 1), 0) # Remove incomplete years
        nrows = len(_[:,0])
        self.years = _[:,0].reshape(nrows, 1)
        self.months = _[:,1].reshape(nrows, 1)
        self.temp = _[:,2].reshape(nrows, 1)
        self.rain = _[:,3].reshape(nrows, 1)
        self.pet = _[:,4].reshape(nrows, 1)


class ClimateDataFromAPI(Climate):
    def __init__(self, latitude: float, longitude: float, date_from: str = "1940-02-01", date_to: str = "2022-01-31") -> None:
        self.latitude = latitude
        self.longitude = longitude
        self.date_from = date_from
        self.date_to = date_to
        self._load_data()

    def _query_open_meteo_api(self, latitude: float, longitude: float, start_date: str = "1940-02-01", end_date: str = "2022-01-31") -> dict:
        api_call = "https://archive-api.open-meteo.com/v1/archive?latitude=" + str(latitude) + "&longitude=" + str(longitude) + "&start_date=" + start_date + "&end_date=" + end_date + "&models=best_match&daily=temperature_2m_mean,rain_sum,et0_fao_evapotranspiration&timezone=auto"
        response = requests.get(api_call)
        r: dict = response.json()
        return r

    def _get_unique_vals_and_preserve_order(self, seq: list) -> list:
        # https://stackoverflow.com/questions/480214/how-do-i-remove-duplicates-from-a-list-while-preserving-order
        seen: set = set()
        seen_add = seen.add
        return [x for x in seq if not (x in seen or seen_add(x))]

    def _format_response_to_monthly(self, api_res: dict) -> np.ndarray:
        full_dates = api_res["daily"]["time"]
        yyyy_mm_dates = [date[:-3] for date in full_dates]
        unique_months = self._get_unique_vals_and_preserve_order(yyyy_mm_dates)
        yyyy_dates = [int(date[:4]) for date in unique_months]
        mm_dates = [int(date[5:]) for date in unique_months]
        temp_daily = api_res["daily"]["temperature_2m_mean"]
        rain_daily = api_res["daily"]["rain_sum"]
        et0_daily = api_res["daily"]["et0_fao_evapotranspiration"]

        temp_monthly, rain_monthly, et0_monthly = [], [], []
        for month in unique_months:
            idx = [i for i, e in enumerate(yyyy_mm_dates) if e == month]
            temp_month_avg = np.mean(itemgetter(*idx)(temp_daily))
            temp_month_round = round(temp_month_avg, 1)
            temp_monthly.append(temp_month_round)
            rain_month_avg = np.sum(itemgetter(*idx)(rain_daily))
            rain_month_mean = round(rain_month_avg, 1)
            rain_monthly.append(rain_month_mean)
            et0_month_avg = np.sum(itemgetter(*idx)(et0_daily))
            et0_month_round = round(et0_month_avg, 1)
            et0_monthly.append(et0_month_round)
        formatted = a([yyyy_dates, mm_dates, temp_monthly, rain_monthly, et0_monthly]).transpose()
        return formatted

    def _load_data(self) -> None:
        api_res = self._query_open_meteo_api(latitude=self.latitude, longitude=self.longitude, start_date=self.date_from, end_date=self.date_to)
        formatted_response = self._format_response_to_monthly(api_res=api_res)
        nrows = len(formatted_response[:,0])
        self.years = formatted_response[:,0].reshape(nrows, 1)
        self.months = formatted_response[:,1].reshape(nrows, 1)
        self.temp = formatted_response[:,2].reshape(nrows, 1)
        self.rain = formatted_response[:,3].reshape(nrows, 1)
        self.pet = formatted_response[:,4].reshape(nrows, 1)


class ClimateDataSampled(Climate):
    def __init__(self, climate_data: Climate, years: int, iters: int) -> None:
        self.climate_data = climate_data
        self._years = years
        self._iters = iters
        self._load_data()

    def _load_data(self) -> None:
        generated_climate_data = self.__generate_climate_data(self.climate_data)
        self.years = np.array(self.__generate_years())
        self.months = np.array(self.__generate_months())
        self.temp = generated_climate_data[0]
        self.rain = generated_climate_data[2]
        self.pet = generated_climate_data[1]

    def __generate_years(self) -> list:
        return [yr for yr in range(self._years) for mn in range(1, 13, 1)]

    def __generate_months(self) -> list:
        return [mn for yr in range(self._years) for mn in range(1, 13, 1)]

    def __collect_values_by_month(self, values: list) -> dict[str, list]:
        months = MONTHS[6:]
        _months = MONTHS[:6]
        months.extend(_months)
        month_vals = {months[i]: [values[k][0] for k in range(i, len(values), 12)] for i in range(12)}
        return month_vals

    def __limit_array_to_bounds(self, array: np.ndarray, lower: float = 0.0, upper: float = 1.0) -> np.ndarray:
        array[array>upper] = array[array>upper] - (array[array>upper] - upper) - np.random.triangular(left=0.01, mode=0.05, right=0.1, size=len(array[array>upper]))
        array[array<lower] = array[array<lower] + (lower - array[array<lower]) + np.random.triangular(left=0.01, mode=0.05, right=0.1, size=len(array[array<lower]))
        return array

    def __generate_walking_quantiles(self, shape: tuple) -> np.ndarray: 
        rows = shape[0]
        iters = shape[1]
        qs: np.ndarray = np.random.triangular(left=0.3, mode=0.5, right=0.7, size=iters)
        for _ in range(rows-1):
            q_modifier = np.random.uniform(low=-0.1, high=0.1, size=iters)
            q_updated_uncapped = qs[-1] + q_modifier
            q_updated = self.__limit_array_to_bounds(array=q_updated_uncapped)
            qs = np.vstack((qs, q_updated))
        return qs

    def __generate_normal_dist_samples(self, values: list, sample_size: int = 1000) -> np.ndarray:
        mu, sigma = np.mean(values), np.std(values)
        return np.random.normal(loc=mu, scale=sigma, size=sample_size)

    def __generate_tri_dist_samples(self, values: list, sample_size: int = 1000) -> np.ndarray:
        _min, _max, _med = min(values), max(values), np.median(values)
        return np.random.triangular(left=_min, mode=_med, right=_max, size=sample_size)

    def __interleave_list_of_arrays(self, list_of_arrays: List[np.ndarray]) -> np.ndarray:
        len_list_of_arrays = len(list_of_arrays)
        len_of_arrays = len(list_of_arrays[0])

        if list_of_arrays[0].ndim == 1: # Conditional added to fix bug if just one year and more than one iteration
            len_of_arrays = 1

        interleaved = [None] * (len_list_of_arrays * len_of_arrays)
        _ = 0
        for i in range(len_of_arrays):
            for m in range(len_list_of_arrays):
                interleaved[_] = list_of_arrays[m][i] if list_of_arrays[0].ndim > 1 else list_of_arrays[m] # Conditional added to fix bug if just one year and more than one iteration
                _ += 1
        interleaved_array = np.array(interleaved)
        return interleaved_array

    def __generate_climate_data(self, climate_data: Climate) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
        temp = self.__collect_values_by_month(climate_data.temp.tolist())
        rain = self.__collect_values_by_month(climate_data.rain.tolist())
        pet = self.__collect_values_by_month(climate_data.pet.tolist())
        qs = self.__generate_walking_quantiles(shape=(self._years,self._iters))
        temp_samples = {month:self.__generate_normal_dist_samples(values=temp[month]) for month in MONTHS}
        pet_samples = {month:self.__generate_normal_dist_samples(values=pet[month]) for month in MONTHS}
        rain_samples = {month:self.__generate_tri_dist_samples(values=rain[month]) for month in MONTHS}
        temp_qs = [np.quantile(temp_samples[month],qs) for month in MONTHS]
        pet_qs = [np.quantile(pet_samples[month],qs) for month in MONTHS]
        rain_qs = [np.quantile(rain_samples[month],qs) for month in MONTHS]
        temp_sim = self.__interleave_list_of_arrays(temp_qs)
        pet_sim = self.__interleave_list_of_arrays(pet_qs)
        rain_sim = self.__interleave_list_of_arrays(rain_qs)
        return temp_sim, pet_sim, rain_sim
