import numpy as np
from numpy import array as a

K = {
    "DPM": a([10]),
    "RPM": a([0.3]),
    "BIO": a([0.66]),
    "HUM": a([0.02])
}

DPM_RPM_RATIO = {
    "agricultural_crops": a([1.44]),
    "improved_grassland": a([1.44]),
    "unimproved_grassland": a([0.67]),
    "scrub": a([0.67]),
    "savanna": a([0.67]),
    "deciduous_woodland": a([0.25]),
    "tropical_woodland": a([0.25])
}

SPLIT_BIO_HUM = {
    "BIO": a([0.46]),
    "HUM": a([0.54])
}

SPLIT_FYM_DPM_RPM_HUM = {
    "DPM": a([0.49]),
    "RPM": a([0.49]),
    "HUM": a([0.02])
}


def y_decomposed(y: np.ndarray, a: np.ndarray, b: np.ndarray, c: np.ndarray, k: np.ndarray, t: np.ndarray = a([1/12])) -> np.ndarray:
    """The amount of material in a compartment that decomposes in a particular month. RothC - A model for the turnover of carbon in soil, Rothampsted Research, Page 9.

    Args:
        y (np.ndarray): Active compartment content (t C ha-1)
        a (np.ndarray): Rate modifying factor for temperature (dimensionless)
        b (np.ndarray): Rate modifying factor for moisture  (dimensionless)
        c (np.ndarray): Soil cover rate modifying factor  (dimensionless)
        k (np.ndarray): Decomposition rate constant for that compartment
        t (np.ndarray, optional): 1/12, since k is based on a yearly decomposition rate. Defaults to a([1/12]).

    Returns:
        np.ndarray: _description_
    """
    _: np.ndarray = y * (1 - np.exp(-1 * a * b * c * k * t)) # replaced c with a([1])
    return _


def rmf_a(T: np.ndarray) -> np.ndarray:
    """Rate modifying factor for temperature (a). RothC - A model for the turnover of carbon in soil, Rothampsted Research, Page 10.

    Args:
        T (np.ndarray): Average monthly air temperature (degrees C).

    Returns:
        np.ndarray: Rate modifying factor for temperature (a) (dimensionless)
    """
    _: np.ndarray = 47.91 / (1 + np.exp(106.06/(T+18.27)))
    return _


def b_soilr(vegetated: np.ndarray) -> np.ndarray:
    """The maximum TSMD obtained using max_tsmd_soilr is that under actively growing vegetation: if the soil is bare during a particular month, this maximum is divided by the value returned by this function to give the bare SMD, to allow for the reduced evaporation from a bare soil. When the soil is bare it is not allowed to dry out further than the bare SMD, unless the accumulated TSMD is already less than the bare SMD in which case it cannot dry out any further. RothC - A model for the turnover of carbon in soil, Rothampsted Research, Page 11.
    Implemented following the approach used by SoilR.

    Args:
        vegetated (np.ndarray): Are growing plants present on the soil? (bool)

    Returns:
        np.ndarray: B (dimensionless) (used in max_tsmd_soilr).
    """
    _: np.ndarray = np.where(vegetated, 1, 1.8)
    return _


def max_tsmd_soilr(B: np.ndarray, clay: np.ndarray = a([33]), thickness: np.ndarray = a([30])) -> np.ndarray:
    """Maximum topsoil moisture defecit. RothC - A model for the turnover of carbon in soil, Rothampsted Research, Page 11.
    Implemented following the approach used by SoilR.

    Args:
        B (np.ndarray): Max TSMD B factor (dimensionless) (see b_soilr).
        clay (np.ndarray): Clay content of the soil (%, 0-100). Defaults to a([33])
        thickness (np.ndarray): Layer thickness (cm). Defaults to a([30]).

    Returns:
        np.ndarray: Maximum TSMD (mm month-1).
    """
    _: np.ndarray = -(20 + 1.3 * clay - 0.01 * (clay**2)) * (thickness/23) * (1/B)
    return _


def m_soilr(precip: np.ndarray, evap: np.ndarray, p_e: np.ndarray = a([1])) -> np.ndarray:
    """Calculate effective rainfall (rainfall - potential evapotranspiration). RothC - A model for the turnover of carbon in soil, Rothampsted Research, Page 13.
    Implemented following the approach used by SoilR.
    
    Args:
        precip (np.ndarray): Precipitation (mm month-1)
        evap (np.ndarray): Potential evapotranspiration (mm month-1)
        p_e (np.ndarray, optional): If evap is a measure of Open Pan Evaporation (OPE) p_e should be set to a([0.75]), else a([1]). Defaults to a([1]) (meaning that the default assumption is that the value for evap is a measure of potential evapotranspiration).

    Returns:
        np.ndarray: Effective rainfall (mm month-1)
    """
    _: np.ndarray = precip - evap * p_e
    return _


def acc_tsmd_soilr(m: np.ndarray, max_tsmd: np.ndarray) -> np.ndarray:
    """Calculate accumulated topsoil moisture defecit (TSMD) given effective rainfall data. RothC - A model for the turnover of carbon in soil, Rothampsted Research, Page 12, 13.
    Implemented following the approach used by SoilR.
    
    Args:
        m (np.ndarray): Effective rainfall (mm month-1)
        max_tsmd (np.ndarray): Maximum TSMD (mm month-1)

    Returns:
        np.ndarray: Accumulated TSMD (mm month-1).
    """
    acc_tsmd = [np.array(None)] * len(m)
    acc_tsmd[0] = np.where(m[0] > 0, 0, m[0])
    acc_tsmd[0] = np.where(acc_tsmd[0] <= max_tsmd[0], max_tsmd[0], acc_tsmd[0])
    for i in range(1, len(m), 1):
        acc_tsmd[i] = np.where(acc_tsmd[i-1] + m[i] < 0, acc_tsmd[i-1] + m[i], 0)
        acc_tsmd[i] = np.where(acc_tsmd[i] <= max_tsmd[i], max_tsmd[i], acc_tsmd[i])
    return np.array(acc_tsmd)


def rmf_b_soilr(acc_tsmd: np.ndarray, max_tsmd: np.ndarray) -> np.ndarray:
    """Calculate the rate modifying factor (b). RothC - A model for the turnover of carbon in soil, Rothampsted Research, Page 14.
    Implemented following the approach used by SoilR.

    Args:
        acc_tsmd (np.ndarray): Accumulated TSMD (mm month-1)
        max_tsmd (np.ndarray): Maximum TSMD (mm month-1)

    Returns:
        np.ndarray: Rate modifying factor (b) (dimensionless)
    """
    b = np.where(acc_tsmd > 0.444 * max_tsmd, 1, (0.2 + 0.8 * ((max_tsmd - acc_tsmd) / (max_tsmd - 0.444 * max_tsmd))))
    return b


def rmf_c(vegetated: np.ndarray) -> np.ndarray:
    """Soil cover factor. RothC - A model for the turnover of carbon in soil, Rothampsted Research, Page 15.

    Args:
        vegetated (np.ndarray): Are growing plants present on the soil? (bool)

    Returns:
        np.ndarray: Soil cover factor (dimensionless)
    """
    _: np.ndarray = np.empty(shape=vegetated.shape)
    _[vegetated] = 0.6
    _[np.invert(vegetated)] = 1
    return _


def co2_bio_hum_ratio_x(perc_clay: np.ndarray) -> np.ndarray:
    """Ratio of CO2 to (BIO + HUM). RothC - A model for the turnover of carbon in soil, Rothampsted Research, Page 15.

    Args:
        perc_clay (np.ndarray): Clay content of the soil (%, 0-100)

    Returns:
        np.ndarray: Ratio of CO2 to (BIO + HUM) (dimensionless)
    """
    _: np.ndarray = 1.67 * (1.85 + 1.6 * np.exp(-0.0786 * perc_clay))
    return _


def iom(toc: np.ndarray) -> np.ndarray:
    """Estimate of intert organic matter from total organic carbon. RothC - A model for the turnover of carbon in soil, Rothampsted Research, Page 18.

    Args:
        toc (np.ndarray): Total organic carbon (t C ha-1)

    Returns:
        np.ndarray: Inert organic matter (t C ha-1)
    """
    _: np.ndarray = 0.049 * toc ** 1.139
    return _
