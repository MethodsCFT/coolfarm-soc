import numpy as np
from numpy import array as a
import cfasoc.rothc.equations as eq
from cfasoc.ipcc.model import create_res
from cfasoc.results import RothCResults
import cfasoc.builders as bd


def calc_rmf_a(temperature: np.ndarray) -> np.ndarray:
    """Calculate the rate modifying factor (a) (temperature)

    Args:
        temperature (np.ndarray): Average monthly air temperature (degrees C).

    Returns:
        np.ndarray: Rate modifying factor for temperature (a) (dimensionless)
    """
    return eq.rmf_a(T=temperature)


def calc_rmf_b(
        precip: np.ndarray,
        pet: np.ndarray,
        vegetated: np.ndarray,
        clay: np.ndarray = a([33]),
        thickness: np.ndarray = a([30]),
        p_e: np.ndarray = a([1])
        ) -> np.ndarray:
    """Calculate the rate modifying factor (b) (moisture) using the implementation approach from SoilR.

    Args:
        precip (np.ndarray): Precipitation (mm month-1)
        pet (np.ndarray): Potential evapotranspiration (mm month-1)
        vegetated (np.ndarray): Are growing plants present on the soil? (bool)
        clay (np.ndarray, optional): Clay content of the soil (%, 0-100). Defaults to a([33])
        thickness (np.ndarray, optional): Layer thickness (cm). Defaults to a([30]).
        p_e (np.ndarray, optional): If evap is a measure of Open Pan Evaporation (OPE) p_e should be set to a([0.75]), else a([1]). Defaults to a([1]) (meaning that the default assumption is that the value for evap is a measure of potential evapotranspiration). Defaults to a([1]).

    Returns:
        np.ndarray: Rate modifying factor (b) (dimensionless)
    """
    b = eq.b_soilr(vegetated=vegetated)
    max_tsmd = eq.max_tsmd_soilr(B=b, clay=clay,thickness=thickness)
    effective_rainfall = eq.m_soilr(precip=precip, evap=pet, p_e=p_e)
    acc_tsmd = eq.acc_tsmd_soilr(m=effective_rainfall, max_tsmd=max_tsmd)
    rmf_b = eq.rmf_b_soilr(acc_tsmd=acc_tsmd, max_tsmd=max_tsmd)
    return rmf_b


def calc_rmf_c(vegetated: np.ndarray) -> np.ndarray:
    """Calculate the rate modifying factor (c) (vegetation).

    Args:
        vegetated (np.ndarray): Are growing plants present on the soil? (bool)

    Returns:
        np.ndarray: Soil cover factor (dimensionless)
    """
    return eq.rmf_c(vegetated=vegetated)


def first_value_from_ratio(ratio: np.ndarray) -> np.ndarray:
    """Get the multiplier to calculate the first value given a ratio.

    Args:
        ratio (np.ndarray): Ratio (dimensionless)

    Returns:
        np.ndarray: Multiplier (dimensionless)
    """
    _: np.ndarray = ratio / (ratio + 1)
    return _


def second_value_from_ratio(ratio: np.ndarray) -> np.ndarray:
    """Get the multiplier to calculate the first value given a ratio.

    Args:
        ratio (np.ndarray): Ratio (dimensionless)

    Returns:
        np.ndarray: Multiplier (dimensionless)
    """
    _: np.ndarray = 1 / (ratio + 1)
    return _


def run(
    c_input: np.ndarray,
    rmf_a: np.ndarray,
    rmf_b: np.ndarray,
    rmf_c: np.ndarray,
    fym: np.ndarray,
    timestamps: list = None,
    dpm_rpm_ratio: np.ndarray = a([1.44]),
    clay: np.ndarray = a([33]),
    init_dpm: np.ndarray = a([0]),
    init_rpm: np.ndarray = a([0]),
    init_bio: np.ndarray = a([0]),
    init_hum: np.ndarray = a([0]),
    init_iom: np.ndarray = a([0]),
    dpm_k: np.ndarray = a([10]),
    rpm_k: np.ndarray = a([0.3]),
    bio_k: np.ndarray = a([0.66]),
    hum_k: np.ndarray = a([0.02]),
    t: np.ndarray = a([1/12]),
    annualise: bool = False
    ) -> RothCResults:
    """Run the RothC model. RothC - A model for the turnover of carbon in soil, Rothampsted Research.

    Args:
        c_input (np.ndarray): Monthly carbon input from crops (t C month-1).
        rmf_a (np.ndarray): Rate modifying factor (a) (dimensionless).
        rmf_b (np.ndarray): Rate modifying factor (b) (dimensionless).
        rmf_c (np.ndarray): Rate modifying factor (c) (dimensionless).
        fym (np.ndarray): Monthly carbon input from farmyard manure (t C month-1).
        timestamps (list, optional): List of timestamps e.g. [January 2020, February 2020]. Defaults to a sequence of 1 to N.
        dpm_rpm_ratio (np.ndarray, optional): Ratio of decomposable plant material (DPM) to resistant plant material (RPM). Defaults to a([1.44]).
        clay (np.ndarray, optional): Clay percentage (0-100) of the soil (percentage). Defaults to a([33]).
        init_dpm (np.ndarray, optional): Initial value of the DPM pool (t C). Defaults to a([0]).
        init_rpm (np.ndarray, optional): Initial value of the RPM pool (t C). Defaults to a([0]).
        init_bio (np.ndarray, optional): Initial value of the BIO pool (t C). Defaults to a([0]).
        init_hum (np.ndarray, optional): Initial value of the HUM pool (t C). Defaults to a([0]).
        init_iom (np.ndarray, optional): Initial value of the IOM pool (t C). Defaults to a([0]).
        dpm_k (np.ndarray, optional): Decomposition rate constant for the DPM pool (dimensionless). Defaults to a([10]).
        rpm_k (np.ndarray, optional): Decomposition rate constant for the RPM pool (dimensionless). Defaults to a([0.3]).
        bio_k (np.ndarray, optional): Decomposition rate constant for the BIO pool (dimensionless). Defaults to a([0.66]).
        hum_k (np.ndarray, optional): Decomposition rate constant for the HUM pool (dimensionless). Defaults to a([0.02]).
        t (np.ndarray, optional): Time (t is 1 / 12, since k is based on a yearly decomposition rate). Defaults to a([1/12]).
        annualise (bool, optional): Should monthly results be averaged to give annual results? Defaults to False.
        
    Returns:
        RothCResults: RothCResults class
    """
    # Reshape input arrays if required
    _ = {"c_input": c_input, "rmf_a": rmf_a, "rmf_b": rmf_b, "rmf_c": rmf_c,
         "fym": fym, "dpm_rpm_ratio": dpm_rpm_ratio, "clay": clay, "dpm_k": dpm_k,
         "rpm_k": rpm_k, "bio_k": bio_k,"hum_k": hum_k}
    inputs = {k:bd.repeat_single(rmf_a.shape,float(v)) if v.ndim==1 else v for k, v in _.items()}
    co2_bio_hum_ratio = eq.co2_bio_hum_ratio_x(perc_clay=inputs["clay"])

    # Initialise arrays to store results
    dpm_res = create_res(template=rmf_b)
    rpm_res = create_res(template=rmf_b)
    bio_res = create_res(template=rmf_b)
    hum_res = create_res(template=rmf_b)
    iom_res = create_res(template=rmf_b)
    iom_res[:] = init_iom #FIXME: Will this work if len(init_iom) > 1?

    # Run for each timestep
    for _ in range(0, len(dpm_res), 1):
        if _ == 0:
            dpm_prev, rpm_prev, bio_prev, hum_prev = init_dpm, init_rpm, init_bio, init_hum
        else:
            dpm_prev, rpm_prev, bio_prev, hum_prev = dpm_res[_-1], rpm_res[_-1], bio_res[_-1], hum_res[_-1]

        dpm_res[_] = dpm_prev + (inputs["c_input"][_] * first_value_from_ratio(inputs["dpm_rpm_ratio"][_])) + (inputs["fym"][_] * eq.SPLIT_FYM_DPM_RPM_HUM["DPM"])
        rpm_res[_] = rpm_prev + (inputs["c_input"][_] * second_value_from_ratio(inputs["dpm_rpm_ratio"][_])) + (inputs["fym"][_] * eq.SPLIT_FYM_DPM_RPM_HUM["RPM"])
        bio_res[_] = bio_prev + 0
        hum_res[_] = hum_prev + 0 + (inputs["fym"][_] * eq.SPLIT_FYM_DPM_RPM_HUM["HUM"])
        dpm_decomposed = eq.y_decomposed(y=dpm_res[_], a=rmf_a[_], b=rmf_b[_], c=rmf_c[_], k=inputs["dpm_k"][_], t=t)
        rpm_decomposed = eq.y_decomposed(y=rpm_res[_], a=rmf_a[_], b=rmf_b[_], c=rmf_c[_], k=inputs["rpm_k"][_], t=t)
        bio_decomposed = eq.y_decomposed(y=bio_res[_], a=rmf_a[_], b=rmf_b[_], c=rmf_c[_], k=inputs["bio_k"][_], t=t)
        hum_decomposed = eq.y_decomposed(y=hum_res[_], a=rmf_a[_], b=rmf_b[_], c=rmf_c[_], k=inputs["hum_k"][_], t=t)
        dpm_res[_] = dpm_res[_] - dpm_decomposed
        rpm_res[_] = rpm_res[_] - rpm_decomposed
        bio_res[_] = bio_res[_] - bio_decomposed
        hum_res[_] = hum_res[_] - hum_decomposed
        bio_hum = second_value_from_ratio(co2_bio_hum_ratio[_]) * (dpm_decomposed + rpm_decomposed + bio_decomposed + hum_decomposed)
        bio_res[_] = bio_res[_] + (eq.SPLIT_BIO_HUM["BIO"] * bio_hum)
        hum_res[_] = hum_res[_] + (eq.SPLIT_BIO_HUM["HUM"] * bio_hum)
        
    soc_res = dpm_res + rpm_res + bio_res + hum_res + iom_res

    # Collate in results class
    ts = list(range(soc_res.shape[0])) if timestamps is None else timestamps
    res = RothCResults(timestamps=ts, soc=soc_res, dpm=dpm_res, rpm=rpm_res, bio=bio_res, hum=hum_res, iom=iom_res)
    if annualise:
        res._annualise()
        res.timestamps = list(range(res.soc.shape[0])) if timestamps is None else timestamps
    return res


def spin_up(
    c_input: np.ndarray,
    rmf_a: np.ndarray,
    rmf_b: np.ndarray,
    rmf_c: np.ndarray,
    fym: np.ndarray,
    dpm_rpm_ratio: np.ndarray = a([1.44]),
    clay: np.ndarray = a([33]),
    dpm_k: np.ndarray = a([10]),
    rpm_k: np.ndarray = a([0.3]),
    bio_k: np.ndarray = a([0.66]),
    hum_k: np.ndarray = a([0.02]),
    run_in_years: int = 5, # Years
    spin_up_years: int = 2000, # Years
    average_from_last_years: int = 50
) -> dict:
    
    #TODO: Docstrings
    #TODO: Save spin up pools to a tempfile to prevent requirement to re-run spin_up every time
    #TODO: Is there a better way of running this to equilibrium rather than just doing so for spin_up_years and assuming equilibrium at that point?
    #TODO: Will fail if spin_up_years / run_in_years is not an integer - improve or add error.

    _ = {"c_input": c_input, "rmf_a": rmf_a, "rmf_b": rmf_b, "rmf_c": rmf_c,
         "fym": fym, "dpm_rpm_ratio": dpm_rpm_ratio, "clay": clay, "dpm_k": dpm_k,
         "rpm_k": rpm_k, "bio_k": bio_k, "hum_k": hum_k}
    months_run_in = run_in_years * 12
    inputs = {k:bd.repeat_single(rmf_a.shape,v) if v.ndim==1 else v for k, v in _.items()}

    inputs_run_in = {k:v[0:months_run_in,:] for k, v in inputs.items()}
    repeats = int(spin_up_years / run_in_years)
    spin_up = {k:bd.stack_arrays([inputs_run_in[k] for _ in range(repeats)]) for k in inputs_run_in.keys()}

    res = run(c_input=spin_up["c_input"], rmf_a=spin_up["rmf_a"], rmf_b=spin_up["rmf_b"],
                rmf_c=spin_up["rmf_c"], fym=spin_up["fym"], dpm_rpm_ratio=spin_up["dpm_rpm_ratio"], 
                clay=spin_up["clay"], dpm_k=spin_up["dpm_k"], rpm_k=spin_up["rpm_k"], 
                bio_k=spin_up["bio_k"], hum_k=spin_up["hum_k"])
    
    # Determine init pools and iom and return
    spin_up = {
        "DPM": np.mean(res.dpm[-average_from_last_years*12:,], axis=0),
        "RPM": np.mean(res.rpm[-average_from_last_years*12:,], axis=0),
        "BIO": np.mean(res.bio[-average_from_last_years*12:,], axis=0),
        "HUM": np.mean(res.hum[-average_from_last_years*12:,], axis=0),
    }
    _ = list(spin_up.values())
    spin_up["IOM"] = eq.iom(toc=np.sum(_, axis=0))

    return spin_up 
