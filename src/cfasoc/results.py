import numpy as np
import matplotlib.pyplot as plt
from typing import Union
from cfasoc.builders import grouped_avg


class Results:
    def __init__(self, timestamps: list, soc: np.ndarray) -> None:
        self.timestamps = timestamps
        self.soc = soc

    def plot_percentile(self, variable: str, percentile: Union[float,tuple]) -> None:
        data = getattr(self, variable)
        percentile_data = [np.percentile(data[_], percentile) for _ in range(len(data))]
        plt.xlabel("Timestep")
        plt.ylabel(variable.upper())
        model = "RothC" if "RothC" in self.__str__() else "IPCC" if "IPCC" in self.__str__() else ""
        color = "#bf2365" if model == "RothC" else "#21379a" if model == "IPCC" else "black"
        plt.plot(self.timestamps, percentile_data, color=color)
        #TODO: Add support for annotation/labels and line dashes

class IPCCResults(Results):
    def __init__(self, timestamps: list, soc: np.ndarray, active: np.ndarray, slow: np.ndarray, passive: np.ndarray) -> None:
        super().__init__(timestamps, soc)
        self.active = active
        self.slow = slow
        self.passive = passive


class RothCResults(Results):
    def __init__(self, timestamps: list, soc: np.ndarray, dpm: np.ndarray, rpm: np.ndarray, bio: np.ndarray, hum: np.ndarray, iom: np.ndarray) -> None:
        super().__init__(timestamps, soc)
        self.dpm = dpm
        self.rpm = rpm
        self.bio = bio
        self.hum = hum
        self.iom = iom

    def _annualise(self) -> None:
        self.soc = grouped_avg(self.soc, 12)
        self.dpm = grouped_avg(self.dpm, 12)
        self.rpm = grouped_avg(self.rpm, 12)
        self.bio = grouped_avg(self.bio, 12)
        self.hum = grouped_avg(self.hum, 12)
        self.iom = grouped_avg(self.iom, 12)
