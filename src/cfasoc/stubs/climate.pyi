from cfasoc.ipcc import CLIMATE_DATA as CLIMATE_DATA
from typing import Any

MONTHS: Any

class Climate:
    years: Any
    months: Any
    temp: Any
    rain: Any
    pet: Any
    def __init__(self) -> None: ...

class DemoClimateData(Climate):
    def __init__(self) -> None: ...

class ClimateDataFromAPI(Climate):
    latitude: Any
    longitude: Any
    date_from: Any
    date_to: Any
    def __init__(self, latitude: float, longitude: float, date_from: str = ..., date_to: str = ...) -> None: ...

class ClimateDataSampled(Climate):
    climate_data: Any
    def __init__(self, climate_data: Climate, years: int, iters: int) -> None: ...
