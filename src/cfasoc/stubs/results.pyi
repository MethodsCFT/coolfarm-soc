import numpy as np
from cfasoc.builders import grouped_avg as grouped_avg
from typing import Any, Union

class Results:
    timestamps: Any
    soc: Any
    def __init__(self, timestamps: list, soc: np.ndarray) -> None: ...
    def plot_percentile(self, variable: str, percentile: Union[float, tuple]) -> None: ...

class IPCCResults(Results):
    active: Any
    slow: Any
    passive: Any
    def __init__(self, timestamps: list, soc: np.ndarray, active: np.ndarray, slow: np.ndarray, passive: np.ndarray) -> None: ...

class RothCResults(Results):
    dpm: Any
    rpm: Any
    bio: Any
    hum: Any
    iom: Any
    def __init__(self, timestamps: list, soc: np.ndarray, dpm: np.ndarray, rpm: np.ndarray, bio: np.ndarray, hum: np.ndarray, iom: np.ndarray) -> None: ...
