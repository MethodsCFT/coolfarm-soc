import numpy as np
from numpy import array as a
from cfasoc.builders import grouped_avg

# TODO: Remove or check support for monthly calculations. IPCC don't do it so why should we? Counter argument here is how to model things like winter cover crops when main crop might be something else. 


def delta_c_mineral(f_soc_i: np.ndarray, a_i: np.ndarray) -> np.ndarray:
    """Equation 5.0A part 1, Annual change in soil C stock for mineral soils using the steady-state method, Page 5.18, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        f_soc_i (np.ndarray): Annual stock change factor for mineral soils in grid cell or region i, tonnes C ha-1.
        a_i (np.ndarray): Area of grid cell or region i, ha.

    Returns:
        np.ndarray: Annual SOC change factor for mineral soil, summed across all i grid cells or regions, tonnes C.
    """
    _: np.ndarray = f_soc_i * a_i
    return _


def f_soc_i(soc_y_i: np.ndarray, soc_y_1_i: np.ndarray) -> np.ndarray:
    """Equation 5.0A part 2, Annual change in soil C stock for mineral soils using the steady-state method, Page 5.18, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        soc_y_i (np.ndarray): SOC stock at the end of the current year y for grid cell or region i, tonnes C ha-1.
        soc_y_1_i (np.ndarray): SOC stock at the end of the previous year for grid cell or region i, tonnes C ha-1.

    Returns:
        np.ndarray: Annual stock change factor for mineral soils in grid cell or region i, tonnes C ha-1.
    """
    _: np.ndarray = soc_y_i - soc_y_1_i
    return _


def soc_y_i(active_y_i: np.ndarray, slow_y_i: np.ndarray, passive_y_i: np.ndarray) -> np.ndarray:
    """Equation 5.0A part 3, Annual change in soil C stock for mineral soils using the steady-state method, Page 5.18, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        active_y_i (np.ndarray): Active sub-pool SOC stock in year y for grid cell or region i, tonnes C ha-1 (see Equation 5.0b).
        slow_y_i (np.ndarray): Slow sub-pool SOC stock in year y for gridd cell or region i, tonnes C ha-1 (see Equation 5.0c).
        passive_y_i (np.ndarray): Passive sub-pool SOC stock in year y for grid cell or region i, tonnes C ha-1 (see Equation 5.0d).

    Returns:
        np.ndarray: SOC stock at the end of the current year y for grid cell or region i, tonnes C ha-1.
    """
    _: np.ndarray = active_y_i + slow_y_i + passive_y_i
    return _


def active_y(active_y_1: np.ndarray, active_y_dot: np.ndarray, k_a: np.ndarray, yr: np.ndarray = a([1])) -> np.ndarray:
    """Equation 5.0B part 1, Active sub-pool soil C stock for mineral soils using the steady-state method, Page 5.19, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        active_y_1 (np.ndarray): Active sub-pool SOC stock in the previous year, tonnes C ha-1
        active_y_dot (np.ndarray): Steady state active sub-pool SOC stock given conditions in year y, tonnes C ha-1
        k_a (np.ndarray): Decay rate for active SOC sub-pool, year-1
        yr (np.ndarray, optional): Year. Defaults to a([1]).

    Returns:
        np.ndarray: Active sub-pool SOC stock in year y, tonnes C ha-1
    """
    k_a[k_a>1] = 1 # Step 7.3, Page 5.25
    _: np.ndarray = active_y_1 + (active_y_dot - active_y_1) * yr * k_a
    return _


def active_y_dot(alpha: np.ndarray, k_a: np.ndarray) -> np.ndarray:
    """Equation 5.0B part 2, Active sub-pool soil C stock for mineral soils using the steady-state method, Page 5.19, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        alpha (np.ndarray): C input to the active SOC sub-pool, tonnes C ha-1 year-1 (see Equation 5.0g)
        k_a (np.ndarray): Decay rate for active SOC sub-pool, year-1

    Returns:
        np.ndarray: Steady state active sub-pool SOC stock given conditions in year y, tonnes C ha-1
    """
    _ : np.ndarray = alpha / k_a
    return _


def k_a(k_fac_a: np.ndarray, t_fac: np.ndarray, w_fac: np.ndarray, sand: np.ndarray, till_fac: np.ndarray) -> np.ndarray:
    """Equation 5.0B part 3, Active sub-pool soil C stock for mineral soils using the steady-state method, Page 5.19, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        k_fac_a (np.ndarray): Decay rate constant under optimal conditions for decomposition of the active SOC sub-pool, year-1 (see Table 5.5a).
        t_fac (np.ndarray): Temperature effect on decomposition, dimensionless (see Equation 5.0e)
        w_fac (np.ndarray): Water effect on decomposition, dimensionless (see Equation 5.0f)
        sand (np.ndarray): Fraction of 0-30cm soil mass that is sand (0.050 - 2mm particles), dimensionless
        till_fac (np.ndarray): Tillage disturbance modifier on decay rate for active and slow sub-pools, dimensionless (see Table 5.5a).

    Returns:
        np.ndarray: Decay rate for active SOC sub-pool, year-1
    """
    _: np.ndarray = k_fac_a * t_fac * w_fac * (0.25 + (0.75 * sand)) * till_fac
    return _


def slow_y(slow_y_1: np.ndarray, slow_y_dot: np.ndarray, k_s: np.ndarray, yr: np.ndarray = a([1])) -> np.ndarray:
    """Equation 5.0C part 1, Slow sub-pool soil C stock for mineral soils using the steady-state method, Page 5.20, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        slow_y_1 (np.ndarray): Slow sub-pool SOC stock in previous year, tonnes C ha-1
        slow_y_dot (np.ndarray): Steady state slow sub-pool SOC stock given conditions in year y, tonnes C ha-1
        k_s (np.ndarray): Decay rate for slow SOC sub-pool, year-1
        yr (np.ndarray): Year. Defaults to a([1]).

    Returns:
        np.ndarray: Slow sub-pool SOC stock in y, tonens C ha-1
    """
    k_s[k_s > 1] = 1 # Step 6.3, Page 5.25
    _: np.ndarray = slow_y_1 + (slow_y_dot - slow_y_1) * yr * k_s
    return _


def slow_y_dot(c_input: np.ndarray, lc: np.ndarray, f_3: np.ndarray, active_y_dot:np.ndarray, k_a: np.ndarray, f_4: np.ndarray, k_s: np.ndarray) -> np.ndarray:
    """Equation 5.0C part 2, Slow sub-pool soil C stock for mineral soils using the steady-state method, Page 5.20, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        c_input (np.ndarray): Total carbon input, tonnes C ha-1 year-1
        lc (np.ndarray): Lignin content of carbon input, proportion (see Table 5.5b and 5.5c for default values, otherwise compile country-specific values)
        f_3 (np.ndarray): Fraction of structural component decay products transferred to the slow sub-pool, proportion (see Table 5.5a)
        active_y_dot (np.ndarray): Steady state active sub-pool SOC stock given conditions in year y, tonnes C ha-1
        k_a (np.ndarray): Decay rate for the active carbon sub-pool in the soil, year-1
        f_4 (np.ndarray): Fraction of active sub-pool decay products transferred to the slow sub-pool, proprtion (see Equation 5.0c)
        k_s (np.ndarray): Decay rate for slow SOC sub-pool, year-1

    Returns:
        np.ndarray: Steady state slow sub-pool SOC stock given conditions in year y, tonnes C ha-1
    """
    _: np.ndarray = (((c_input * lc) * f_3) + ((active_y_dot * k_a) * f_4)) / k_s
    return _


def k_s(k_fac_s: np.ndarray, t_fac: np.ndarray, w_fac: np.ndarray, till_fac: np.ndarray) -> np.ndarray:
    """Equation 5.0C part 3, Slow sub-pool soil C stock for mineral soils using the steady-state method, Page 5.20, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        k_fac_s (np.ndarray): Decay rate constant under optimal condition for decomposition of the slow carbon sub-pool, year-1 (see Table 5.5a)
        t_fac (np.ndarray): Temperature effect on decomposition, dimensionless (see Equation 5.0e)
        w_fac (np.ndarray): Water effect on decomposition, dimensionless (see Equation 5.0f)
        till_fac (np.ndarray): Tillage disturbance modifier on decay rate for active and slow sub-pools, dimensionless (see Table 5.5a).

    Returns:
        np.ndarray: Decay rate for slow SOC sub-pool, year-1
    """
    _: np.ndarray = k_fac_s * t_fac * w_fac * till_fac
    return _


def f_4(f_5: np.ndarray, sand: np.ndarray) -> np.ndarray:
    """Equation 5.0C part 4, Slow sub-pool soil C stock for mineral soils using the steady-state method, Page 5.20, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories
    Args:
        f_5 (np.ndarray): Fraction of active sub-pool decay products transferred to the passive sub-pool, proportion (see Table 5.5a)
        sand (np.ndarray): Fraction of 0-30cm soil mass that is sand (0.050 - 2mm particles), dimensionless

    Returns:
        np.ndarray: Fraction of active sub-pool decay products transferred to the slow sub-pool, proprtion
    """
    _: np.ndarray = 1 - f_5 - (0.17 + 0.68 * sand)
    return _


def passive_y(passive_y_1: np.ndarray, passive_y_dot: np.ndarray, k_p: np.ndarray, yr: np.ndarray = a([1])) -> np.ndarray:
    """Equation 5.0D part 1, Passive sub-pool soil C stock for mineral soils using the steady-state method, Page 5.21, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        passive_y_1 (np.ndarray): Passive sub-pool SOC stock in previous year, tonnes C ha-1
        passive_y_dot (np.ndarray): Steady state passive sub-pool SOC given conditions in year y, tonnes C ha-1
        k_p (np.ndarray): Decay rate for passive SOC sub-pool, year-1
        yr (np.ndarray, optional): Year. Defaults to a([1]).

    Returns:
        np.ndarray: Passive sub-pool SOC stock in year y, tonnes C ha-1
    """
    k_p[k_p > 1] = 1 # Step 5.3, Page 5.25
    _: np.ndarray = passive_y_1 + (passive_y_dot - passive_y_1) * yr * k_p
    return _


def passive_y_dot(active_y_dot: np.ndarray, k_a: np.ndarray, f_5: np.ndarray, slow_y_dot: np.ndarray, k_s: np.ndarray, f_6: np.ndarray, k_p: np.ndarray) -> np.ndarray:
    """Equation 5.0D part 2, Passive sub-pool soil C stock for mineral soils using the steady-state method, Page 5.21, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        active_y_dot (np.ndarray): Steady state active sub-pool SOC stock given conditions in year y, tonnes C ha-1
        k_a (np.ndarray): Decay rate for the active carbon sub-pool in the soil, year-1
        f_5 (np.ndarray): Fraction of active sub-pool decay products transferred to the passive sub-pool, proportion (see Table 5.5a)
        slow_y_dot (np.ndarray): Steady state slow sub-pool SOC stock given conditions in year y, tonnes C ha-1
        k_s (np.ndarray): Decay rate for slow SOC sub-pool, year-1
        f_6 (np.ndarray): Fraction of slow sub-pool decay products transferred to the passive sub-pool, proportion (see Table 5.5a)
        k_p (np.ndarray): Decay rate for passive SOC sub-pool, year-1

    Returns:
        np.ndarray: Steady state passive sub-pool SOC given conditions in year y, tonnes C ha-1
    """
    _: np.ndarray = (((active_y_dot * k_a) * f_5) + ((slow_y_dot * k_s) * f_6)) / k_p
    return _


def k_p(k_fac_p: np.ndarray, t_fac: np.ndarray, w_fac: np.ndarray) -> np.ndarray:
    """Equation 5.0D part 3, Passive sub-pool soil C stock for mineral soils using the steady-state method, Page 5.21, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        k_fac_p (np.ndarray): Decay rate constant under optimal conditions for decomposition of the slow carbon sub-pool, year-1 (see Table 5.5a)
        t_fac (np.ndarray): Temperature effect on decomposition, dimensionless (see Equation 5.0e)
        w_fac (np.ndarray): Water effect on decomposition, dimensionless (see Equation 5.0f)

    Returns:
        np.ndarray: Decay rate for passive SOC sub-pool, year-1
    """
    _: np.ndarray = k_fac_p * t_fac * w_fac
    return _


def t_fac(t_i: np.ndarray, monthly: bool = False) -> np.ndarray:
    """Equation 5.0E part 1, Temperature effect on decomposition for mineral soils using the steady-state method, Page 5.22, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories
    * Modified so that it can also be used to return monthly values. 

    Args:
        t_i (np.ndarray): Monthly average air temperature effect on decomposition, dimensionless
        monthly (bool, optional): Should value be calculated monthly (else annually). Defaults to False (annually).

    Returns:
        np.ndarray: Average air temperature effect on decomposition, dimensionless
    """
    if monthly:
        _a: np.ndarray = t_i
        return _a
    else:
        _b: np.ndarray = grouped_avg(t_i, n=12)
        return _b


def t_i(t_max: np.ndarray, temp_i: np.ndarray, t_opt: np.ndarray) -> np.ndarray:
    """Equation 5.0E part 2, Temperature effect on decomposition for mineral soils using the steady-state method, Page 5.22, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        t_max (np.ndarray): Maximum monthly air temperature for decomposition, degrees C (see Table 5.5a)
        temp_i (np.ndarray): Monthly average air temprature, degrees C
        t_opt (np.ndarray): Optimum air temperature for decomposition, degrees C (see Table 5.5a)

    Returns:
        np.ndarray: Monthly average air temperature effect on decomposition, dimensionless
    """
    _: np.ndarray = (((t_max - temp_i)/(t_max - t_opt))**0.2) * np.exp(0.076 * (1-((t_max - temp_i)/(t_max - t_opt))**2.63))
    return _


def w_fac(w_i: np.ndarray, monthly: bool = False) -> np.ndarray:
    """Equation 5.0F part 1, Water effect on decomposition for mineral soils using the steady-state method, Page 5.22, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories
    * Modified so that it can also be used to return monthly values. 
    Args:
        w_i (np.ndarray): Monthly water effect on decomposition, dimensionless
        monthly (bool, optional): Should value be calculated monthly (else annually). Defaults to False (annually).

    Returns:
        np.ndarray:  Water effect on decomposition, dimensionless
    """
    if monthly:
        _a: np.ndarray = 1.5 * w_i
        return _a
    else:
        _b: np.ndarray = 1.5 * grouped_avg(w_i, n=12)
        return _b


def w_i(w_s: np.ndarray, mappet_i: np.ndarray) -> np.ndarray:
    """Equation 5.0F part 2, Water effect on decomposition for mineral soils using the steady-state method, Page 5.22, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        w_s (np.ndarray): Modifier for mappet_i, dimensionless (see Table 5.5a)
        mappet_i (np.ndarray): Ratio of total precipitation to total potential evapotranspiration (dimensionless) for month i

    Returns:
        np.ndarray: Monthly water effect on decomposition, dimensionless
    """
    _: np.ndarray = 0.2129 + (w_s * mappet_i) - (0.2413 * mappet_i**2)
    return _


def mappet_i(precip_i: np.ndarray, pet_i: np.ndarray) -> np.ndarray:
    """Equation 5.0F part 3, Water effect on decomposition for mineral soils using the steady-state method, Page 5.22, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        precip_i (np.ndarray): Total precipitation for month i, mm
        pet_i (np.ndarray): Total potential evapostranspiration for month i, mm

    Returns:
        np.ndarray: Ratio of total precipitation to total potential evapotranspiration (dimensionless) for month i (i = 1, 2, ..., 12)
    """
    _: np.ndarray = precip_i / pet_i
    _[_ > 1.25] = 1.25
    return _


def alpha(beta: np.ndarray, f_1: np.ndarray, c_input: np.ndarray, lc: np.ndarray, f_2: np.ndarray, f_3: np.ndarray, f_7: np.ndarray, f_6: np.ndarray, f_8: np.ndarray, f_4: np.ndarray, f_5: np.ndarray) -> np.ndarray:
    """Equation 5.0G part 1, C input to the active soil C sub-pool for mineral soils using the steady-state method, Page 5.23, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        beta (np.ndarray): C input to the metabolic dead organic matter C component, tonnes C ha-1 year-1
        f_1 (np.ndarray): Fraction of metabolic dead organic matter decay products transferred to the active sub-pool, proportion (see Table 5.5a)
        c_input (np.ndarray): Total carbon input, tonnes C ha-1 year-1
        lc (np.ndarray): Lignin content of carbon input, proportion (see Tables 5.5b and 5.5c for default values, otherwise compile country-specific values)
        f_2 (np.ndarray): Fraction of structural dead organic matter decay products transferred to the active sub-pool, proportion (see Table 5.5a)
        f_3 (np.ndarray): Fraction of structural dead organic matter decay products transferred to the slow sub-pool, proportion (see Table 5.5a)
        f_7 (np.ndarray): Fraction of slow sub-pool decay products transferred to the active sub-pool, proportion (see Table 5.5a)
        f_6 (np.ndarray): Fraction of slow sub-pool decay products transferred to the passive sub-pool, proportion (see Table 5.5a) 
        f_8 (np.ndarray): Fraction of passive sub-pool decay products transferred to the active sub-pool, proportion (see Table 5.5a)
        f_4 (np.ndarray): Fraction of active sub-pool decay products transferred to the slow sub-pool, proportion (see Table 5.5a)
        f_5 (np.ndarray): Fraction of active sub-pool decay products transferred to the passive sub-pool, proportion (see Table 5.5a)

    Returns:
        np.ndarray: C input to the active soil carbon sub-pool, tonnes C ha-1
    """
    _: np.ndarray = ((beta * f_1) + ((c_input * (1 - lc) - beta) * f_2) + ((c_input * lc) * f_3 * (f_7 + f_6 * f_8))) / (1 - (f_4 * f_7) - (f_5 * f_8) - (f_4 * f_6 * f_8))
    return _


def beta(c_input: np.ndarray, lc: np.ndarray, nc: np.ndarray) -> np.ndarray:
    """Equation 5.0G part 2, C input to the active soil C sub-pool for mineral soils using the steady-state method, Page 5.23, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        c_input (np.ndarray): Total carbon input, tonnes C ha-1 year-1
        lc (np.ndarray): Lignin content of carbon input, proportion (see Tables 5.5b and 5.5c for default values, otherwise compile country-specific values)
        nc (np.ndarray): Nitrogen fraction of the carbon input, proportion (see Tables 5.5b and 5.5c for default values, otherwise compile country-specific values)

    Returns:
        np.ndarray: C input to the metabolic dead organic matter C component, tonnes C ha-1 year-1
    """
    _: np.ndarray = c_input * (0.85 - 0.018 * (lc / nc))
    return _


def c_input(agr_t: np.ndarray, bgr_t: np.ndarray, f_am_t: np.ndarray, cn_am_t: np.ndarray, c_ag_t: np.ndarray = a([0.42]), c_bg_t: np.ndarray = a([0.42])) -> np.ndarray:
    """Equation 5.0H part 1, Cropland c-input to soil for the steady-state method, Page 5.36, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        agr_t (np.ndarray): Annual total amount of above-ground crop residue for crop T, kg d.m. yr-1
        bgr_t (np.ndarray): Annual total amount of belowground crop residue for crop T, kg d.m. yr-1
        f_am_t (np.ndarray): N in animal manures applied to crop T, kg N yr-1 (Equation 10.34 in Section 10.5.4, Chapter 10)
        cn_am_t (np.ndarray): C to N ratio of animal manures applied to crop T, kg C (kg N)-1 (Table 5.5c)
        c_ag_t (np.ndarray, optional): C content of above-ground residues for crop T, kg C (kg d.m.)-1 (default: 0.42 kg C (kg d.m.)-1). Defaults to 0.42.
        c_bg_t (np.ndarray, optional): C content of below-ground residues for crop T, kg C (kg d.m.)-1 (default: 0.42 kg C (kg d.m.)-1). Defaults to 0.42.

    Returns:
        np.ndarray: Annual amount of C input from residues to the soil (above and below ground), kg C yr-1
    """
    _: np.ndarray = (agr_t * c_ag_t) + (bgr_t * c_bg_t) + (f_am_t * cn_am_t)
    return _


def agr_t(ag_dm_t: np.ndarray, area_t: np.ndarray, frac_renew_t: np.ndarray, frac_removal_t: np.ndarray, frac_burnt_t: np.ndarray, c_f: np.ndarray) -> np.ndarray:
    """Equation 5.0H part 2, Cropland c-input to soil for the steady-state method, Page 5.36, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        ag_dm_t (np.ndarray): Above-ground residue dry matter for crop T, kg d.m. ha-1
        area_t (np.ndarray): Total annual area harvested of crop T, ha yr-1
        frac_renew_t (np.ndarray): Fraction of total area under crop T that is renewed annually, dimensionless. For countries where forages are renewed on average every X years, frac_renew_t = 1/X. For annual crops frac_renew_t = 1
        frac_removal_t (np.ndarray): Fraction of above-ground residues of crop T removed annually for purposes such as feed, bedding and construction, dimensionless. Survey of experts in country is required to obtain data. If data for frac_removal_t are not available, assume no removal.
        frac_burnt_t (np.ndarray): Fraction of annual harvested area of crop T burnt, dimensionless.
        c_f (np.ndarray): Combustion factor (dimensionless) (refer to Chapter 2, Table 2.6)

    Returns:
        np.ndarray: Annual total amount of above-ground crop residue for crop T, kg d.m. yr-1
    """
    _: np.ndarray = ag_dm_t * area_t * frac_renew_t * (1 - frac_removal_t - (frac_burnt_t * c_f))
    return _


def bgr_t(crop_t: np.ndarray, rs_t: np.ndarray, area_t: np.ndarray, frac_renew_t: np.ndarray) -> np.ndarray:
    """Equation 5.0H part 3, Cropland c-input to soil for the steady-state method, Page 5.36, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories
    Note: This equation has been modified from the form shown in the original reference because it appears to contain an error. The modification comment shown below shows this equation implemented as per the IPCC documentation, however this implementation does not appear to make sense and produces unrealistic results. The equation implemented here is based on the authors interpretation. 

    Args:
        crop_t (np.ndarray): Harvested annual dry matter yield for crop T, kg d.m. ha-1 (Use Equation 11.7, Chapter 11)
        rs_t (np.ndarray): Ration of below-ground root biomass to above-ground shoot biomass for crop T, kg d.m. ha-1(kg d.m. ha-1)-1, (Table 11.1a)
        area_t (np.ndarray): Total annual area harvested of crop T, ha yr-1
        frac_renew_t (np.ndarray): Fraction of total area under crop T that is renewed annually, dimensionless. For countries where forages are renewed on average every X years, frac_renew_t = 1/X. For annual crops frac_renew_t = 1

    Returns:
        np.ndarray: Annual total amount of belowground crop residue for crop T, kg d.m. yr-1
    """
    # Modification
    # The second argument (1 + ag_dm_t) has been removed as it appears to be erroneous.
    # _: np.ndarray = crop_t * (1 + ag_dm_t) * rs_t * area_t * frac_renew_t
    _: np.ndarray = crop_t * rs_t * area_t * frac_renew_t
    return _


def ag_dm_t(crop_t: np.ndarray, r_ag_t: np.ndarray) -> np.ndarray:
    """Equation 5.0H part 4, Cropland c-input to soil for the steady-state method, Page 5.36, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        crop_t (np.ndarray): Harvested annual dry matter yield for crop T, kg d.m. ha-1 (Use Equation 11.7, Chapter 11)
        r_ag_t (np.ndarray): Ratio of above-ground residues dry matter (ag_dm_t) to harvested yield for crop T (crop_t), kg d.m. ha-1 (kg d.m. ha-1)-1, (Table 11.1a)

    Returns:
        np.ndarray: Above-ground residue dry matter for crop T, kg d.m. ha-1
    """
    _: np.ndarray = crop_t * r_ag_t
    return _


def crop_t(yield_fresh_t: np.ndarray, dry: np.ndarray = a([0.85])) -> np.ndarray:
    """Equation 11.7, Dry-weight correction of reported crop yields, Page 11.16, Chapter 11 N2O Emissions from Managed Soils, and CO2 Emissions from Lime and Urea Application, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        yield_fresh_t (np.ndarray): Harvested fresh yield for crop T, kg fresh weight ha-1
        dry (np.ndarray, optional): Dry matter fraction of harvested crop T, kg d.m. (kg fresh weight)-1. Defaults to 0.85.

    Returns:
        np.ndarray: Harvested annual dry matter yield for crop T, kg d.m. ha-1
    """
    _: np.ndarray = yield_fresh_t * dry
    return _


def n_mms_avb(n_t: np.ndarray, nex_t: np.ndarray, awms_ts: np.ndarray, n_cdg: np.ndarray, frac_lossms_ts: np.ndarray, n_beddingms_ts: np.ndarray) -> np.ndarray:
    """Equation 10.34, Managed manure N available for application to managed soils, feed, fuel or construction uses, Page 10.94, Chapter 10, Emissions from Livestock and Manure Management, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        n_t (np.ndarray): Number of head of livestock species/category T in the country
        nex_t (np.ndarray): Annual average N excretion per animal of species/category T in the country, kg N animal-1 yr-1
        awms_ts (np.ndarray): Fraction of total annual nitrogen excretion for each livestock species/category T that is managed in manure management system S in the country, dimensionless
        n_cdg (np.ndarray): Amount of nitrogen from co-digestates added to biogas plants such as food wastes or purpose grown crops kg N yr-1
        frac_lossms_ts (np.ndarray): Total fraction of managed manure nitrogen for livestock category T that is lost in the manure management system S. FracLossMS is calculated according to Equation 10.34a
        n_beddingms_ts (np.ndarray): Amount of nitrogen from bedding (to be applied for solid storage and deep bedding MMS if known organic bedding usage), kg N animal-1 yr-1

    Returns:
        np.ndarray: Amount of managed manure nitrogen available for application to managed soils or for feed, fuel, or construction purposes, kg N yr-1
    """
    _: np.ndarray = ((n_t * nex_t * awms_ts + n_cdg) * (1 - frac_lossms_ts)) + (n_t * awms_ts * n_beddingms_ts)
    return _


def frac_lossms_ts(frac_gas_ms_ts: np.ndarray, frac_leachs_ms_ts: np.ndarray, frac_n2_ms_s: np.ndarray, ef_3_s: np.ndarray) -> np.ndarray:
    """Equation 10.34A, Fraction of managed manure N lost prior to application to managed soils for the production of feed, fuel or for construction uses, Page 10.94, Chapter 10, Emissions from Livestock and Manure Management, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        frac_gas_ms_ts (np.ndarray): Fraction of managed manure nitrogen for livestock category T that is lost by volatilisation in the manure management system S as NH3 or NOX (see Table 10.22)
        frac_leachs_ms_ts (np.ndarray): Fraction of managed manure nitrogen for livestock category T that is lost in the manure management system S by leaching or run-off (see Table 10.22)
        frac_n2_ms_s (np.ndarray): Fraction of managed manure nitrogen that is lost in the manure management system S as N2 (see Equation 10.34b)
        ef_3_s (np.ndarray): Emission factor for direct N2O emissions from manure management system S; in this case considered dimensionless (see Table 10.21)

    Returns:
        np.ndarray: Total fraction of managed manure nitrogen for livestock category T that is lost in the manure management system S
    """
    _: np.ndarray = frac_gas_ms_ts + frac_leachs_ms_ts + frac_n2_ms_s + ef_3_s
    return _


def frac_n2_ms_s(ef_3_s: np.ndarray, r_n2_n20: np.ndarray = a([3])) -> np.ndarray:
    """Equation 10.34B, Estimation of FracN2MS, Page 10.95, Chapter 10, Emissions from Livestock and Manure Management, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        ef_3_s (np.ndarray): Emission factor for direct N2O emissions from manure management system S in the country, kg N2O-N (kg N)-1 in manure management system S (see Table 10.21)
        r_n2_n20 (np.ndarray, optional): Ratio of N2 : N2O emissions. The default value of RN2(N2O) is 3 kg N2-N (kg N2O-N)-1 (see Table 10.23). Defaults to a([3]).

    Returns:
        np.ndarray: Fraction of managed manure nitrogen that is lost in the manure management system S as N2.
    """
    _: np.ndarray = r_n2_n20 * ef_3_s
    return _


def n2o_n_prp(f_prp_cpp_so: np.ndarray, ef_prp_cpp_so: np.ndarray) -> np.ndarray:
    """Equation 11.1 (part 4 only), Direct N20 emissions from managed soils (Tier 1), Page 11.6, Chapter 11 N2O Emissions from Managed Soils, and CO2 Emissions from Lime and Urea Application, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories
    Note: Only part 4 of this equation is used as per Table 10.21, row 1, columns 2&3: "The manure from pasture and range grazing animals is allowed to lie as is, and is not managed. Direct and indirect N2O emissions associated with the manure deposited on agricultural soils and pasture, range, paddock systems are treated in Chapter 11, Section 11.2, N2O emissions from managed soils."

    Args:
        f_prp_cpp_so (np.ndarray): Annual amount of urine and dung N deposited by grazing animals on pasture, range and paddock, kg N animal-1 yr-1 (Note: the subscripts CPP and SO refer to Cattle, Poultry and Pigs, and Sheep and Other animals, respectively)
        ef_prp_cpp_so (np.ndarray): Emission factor for N2O emissions from urine and dung N deposited on pasture, range and paddock by grazing animals, kg N2O-N (kg N input)-1; (Table 11.1) (Note: the subscripts CPP and SO refer to Cattle, Poultry and Pigs, and Sheep and Other animals, respectively)

    Returns:
        np.ndarray: Annual direct N2O-N emissions from urine and dung inputs to grazed soils, kg N2O-N yr-1
    """
    _: np.ndarray = f_prp_cpp_so * ef_prp_cpp_so
    return _
