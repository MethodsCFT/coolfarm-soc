import os
import json
import numpy as np

MODULE_DIR = os.path.dirname(os.path.realpath(__file__))
MODULE_DATA_DIR = os.path.join(MODULE_DIR, 'data')

_ = os.path.join(MODULE_DATA_DIR, 'Table2.6.json')
with open(_) as f:
    TABLE_2_6 = json.load(f)

_ = os.path.join(MODULE_DATA_DIR, 'Table5.5A.json')
with open(_) as f:
    TABLE_5_5_A = json.load(f)

_ = os.path.join(MODULE_DATA_DIR, 'Table5.5B.json')
with open(_) as f:
    TABLE_5_5_B = json.load(f)

_ = os.path.join(MODULE_DATA_DIR, 'Table5.5C.json')
with open(_) as f:
    TABLE_5_5_C = json.load(f)

_ = os.path.join(MODULE_DATA_DIR, 'Table10.19.json')
with open(_) as f:
    TABLE_10_19 = json.load(f)

_ = os.path.join(MODULE_DATA_DIR, 'Table10.21.json')
with open(_) as f:
    TABLE_10_21 = json.load(f)

_ = os.path.join(MODULE_DATA_DIR, 'Table10.22.json')
with open(_) as f:
    TABLE_10_22 = json.load(f)

_ = os.path.join(MODULE_DATA_DIR, 'Table10.23.json')
with open(_) as f:
    TABLE_10_23 = json.load(f)

_ = os.path.join(MODULE_DATA_DIR, 'Table11.1A.json')
with open(_) as f:
    TABLE_11_1_A = json.load(f)

_ = os.path.join(MODULE_DATA_DIR, 'ipcc_climate_data.csv')
CLIMATE_DATA = np.genfromtxt(_, delimiter = ",")
