import numpy as np
from numpy import array as a
import cfasoc.ipcc.equations as eq
from cfasoc.results import IPCCResults


def calc_t_fac(t_max: np.ndarray, temp_i: np.ndarray, t_opt: np.ndarray, monthly: bool = False) -> np.ndarray:
    """Step 1.1, part a, calculate the initial stocks of the active, slow and passive SOC sub-pools, Page 5.24, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories
    Calculate the average annual values of t_fac and w_fac

    Args:
        t_max (np.ndarray): Maximum monthly air temperature for decomposition, degrees C (see Table 5.5a)
        temp_i (np.ndarray): Monthly average air temprature, degrees C
        t_opt (np.ndarray): Optimum air temperature for decomposition, degrees C (see Table 5.5a)
        monthly (bool, optional): Should value be calculated monthly (else annually). Defaults to False (annually). Defaults to False.

    Returns:
        np.ndarray: Average air temperature effect on decomposition, dimensionless
    """
    t_i = eq.t_i(t_max=t_max, temp_i=temp_i, t_opt=t_opt)
    t_i[temp_i > 45] = 0 # Step 4.1, page 5.25
    t_fac = eq.t_fac(t_i=t_i, monthly=monthly)
    return t_fac


def calc_w_fac(precip_i: np.ndarray, pet_i: np.ndarray, w_s: np.ndarray, irrigated: np.ndarray, monthly: bool = False) -> np.ndarray:
    """Step 1.1, part b, calculate the initial stocks of the active, slow and passive SOC sub-pools, Page 5.24, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories
    Calculate the average annual values of t_fac and w_fac

    Args:
        precip_i (np.ndarray): Total precipitation for month i, mm
        pet_i (np.ndarray): Total potential evapostranspiration for month i, mm
        w_s (np.ndarray): Modifier for mappet_i, dimensionless (see Table 5.5a)
        irrigated (np.ndarray): is the month irrigated (True) or not (False)
        monthly (bool, optional): Should value be calculated monthly (else annually). Defaults to False (annually). Defaults to False.

    Returns:
        np.ndarray: Water effect on decomposition, dimensionless
    """
    mappet_i = eq.mappet_i(precip_i=precip_i, pet_i=pet_i)
    w_i = eq.w_i(w_s=w_s, mappet_i=mappet_i)
    w_i[irrigated] = 0.775 # Step 3.2, page 5.24
    w_fac = eq.w_fac(w_i=w_i, monthly=monthly)
    return w_fac


def calc_c_input(yield_fresh_t: np.ndarray, dry: np.ndarray = a([0.85]), r_ag_t: np.ndarray = a([1]), rs_t: np.ndarray = a([0.22]), area_t: np.ndarray = a([1]), frac_renew_t: np.ndarray = a([1]), frac_removal_t: np.ndarray = a([0]), frac_burnt_t: np.ndarray = a([0]), c_f: np.ndarray = a([0.5]), f_am_t: np.ndarray = a([0]), cn_am_t: np.ndarray = a([0]), c_ag_t: np.ndarray = a([0.42]), c_bg_t: np.ndarray = a([0.42])) -> np.ndarray:
    """Step 1.2, part a, calculate the initial stocks of the active, slow and passive SOC sub-pools, Page 5.24, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories
    Calculate the average annual carbon input

    Args:
        yield_fresh_t (np.ndarray): Harvested fresh yield for crop T, kg fresh weight ha-1
        dry (np.ndarray): Dry matter fraction of harvested crop T, kg d.m. (kg fresh weight)-1. Defaults to a([0.85]).
        r_ag_t (np.ndarray): Ratio of above-ground residues dry matter (ag_dm_t) to harvested yield for crop T (crop_t), kg d.m. ha-1 (kg d.m. ha-1)-1, (Table 11.1a). Defaults to a([1]).
        rs_t (np.ndarray): Ratio of below-ground root biomass to above-ground shoot biomass for crop T, kg d.m. ha-1(kg d.m. ha-1)-1, (Table 11.1a). Defaults to a([0.22]).
        area_t (np.ndarray): Total annual area harvested of crop T, ha yr-1. Defaults to a([1]).
        frac_renew_t (np.ndarray): Fraction of total area under crop T that is renewed annually, dimensionless. For countries where forages are renewed on average every X years, frac_renew_t = 1/X. For annual crops frac_renew_t = 1. Defaults to a([1]).
        frac_removal_t (np.ndarray): Fraction of above-ground residues of crop T removed annually for purposes such as feed, bedding and construction, dimensionless. Survey of experts in country is required to obtain data. If data for frac_removal_t are not available, assume no removal. Defaults to a([0]).
        frac_burnt_t (np.ndarray): Fraction of annual harvested area of crop T burnt, dimensionless. Defaults to a([0]).
        c_f (np.ndarray): Combustion factor (dimensionless) (refer to Chapter 2, Table 2.6). Defaults to a([0.5]).
        f_am_t (np.ndarray): N in animal manures applied to crop T, kg N yr-1 (Equation 10.34 in Section 10.5.4, Chapter 10). Defaults to a([0]) (no manures applied).
        cn_am_t (np.ndarray): C to N ratio of animal manures applied to crop T, kg C (kg N)-1 (Table 5.5c). Defaults to a([0]) (no manures applied).
        c_ag_t (np.ndarray): C content of above-ground residues for crop T, kg C (kg d.m.)-1 (default: 0.42 kg C (kg d.m.)-1). Defaults to a([0.42]).
        c_bg_t (np.ndarray): C content of below-ground residues for crop T, kg C (kg d.m.)-1 (default: 0.42 kg C (kg d.m.)-1). Defaults to a([0.42]).

    Returns:
        np.ndarray: C input to the active soil carbon sub-pool, kg C yr-1
    """
    crop_t = eq.crop_t(yield_fresh_t=yield_fresh_t, dry=dry)
    ag_dm_t = eq.ag_dm_t(crop_t=crop_t, r_ag_t=r_ag_t)
    bgr_t = eq.bgr_t(crop_t=crop_t, rs_t=rs_t, area_t=area_t, frac_renew_t=frac_renew_t)
    agr_t = eq.agr_t(ag_dm_t=ag_dm_t, area_t=area_t, frac_renew_t=frac_renew_t, frac_removal_t=frac_removal_t, frac_burnt_t=frac_burnt_t, c_f=c_f)
    c_input = eq.c_input(agr_t=agr_t, bgr_t=bgr_t, f_am_t=f_am_t, cn_am_t=cn_am_t, c_ag_t=c_ag_t, c_bg_t=c_bg_t)
    return c_input


def create_res(template: np.ndarray) -> np.ndarray:
    """Create results array to host active, slow and passive pool results

    Args:
        template (np.ndarray): Template np.ndarray to use

    Returns:
        np.ndarray: Empty array ready to store results in
    """
    res = np.empty((template.shape))
    res[:] = np.nan
    return res


def run(
    t_fac: np.ndarray,
    w_fac: np.ndarray,
    c_input: np.ndarray,
    timestamps: list = None,
    active_init: np.ndarray = None,
    passive_init: np.ndarray = None,
    slow_init: np.ndarray = None,
    run_in_period: int = 5,
    sand: np.ndarray = a([0.33]),
    lc: np.ndarray = a([0.073]),
    nc: np.ndarray = a([0.0083]),
    till_fac: np.ndarray = a([3.036]),
    f_1: np.ndarray = a([0.378]),
    f_2: np.ndarray = a([0.368]),
    f_3: np.ndarray = a([0.455]),
    f_5: np.ndarray = a([0.0855]),
    f_6: np.ndarray = a([0.0504]),
    f_7: np.ndarray = a([0.42]),
    f_8: np.ndarray = a([0.45]),
    k_fac_a: np.ndarray = a([7.4]),
    k_fac_s: np.ndarray = a([0.209]),
    k_fac_p: np.ndarray = a([0.00689])
    ) -> IPCCResults:
    """Run the SOC model as per the guidance on Page 5.24, Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories

    Args:
        t_fac (np.ndarray): Temperature effect on decomposition, dimensionless (see Equation 5.0e)
        w_fac (np.ndarray): Water effect on decomposition, dimensionless (see Equation 5.0f)
        c_input (np.ndarray): Total carbon input, tonnes C ha-1 year-1
        timestamps (list, optional): List of timestamps e.g. [January 2020, February 2020]. Defaults to a sequence of 1 to N.
        active_init (np.ndarray, optional): The initial value of the active carbon pool. Default: the initial value of the active C pool for the model is set based on the average steady-state value for run_in_peroid years prior to the first year in the inventory time series.
        passive_init (np.ndarray, optional): The initial value of the passive carbon pool. Default: the initial value of the passive C pool for the model is set based on the average steady-state value for run_in_peroid years prior to the first year in the inventory time series.
        slow_init (np.ndarray, optional): The initial value of the slow carbon pool. Default: the initial value of the slow C pool for the model is set based on the average steady-state value for run_in_peroid years prior to the first year in the inventory time series.
        run_in_period (int, optional): Number of years used to define the initial value of the active, slow and passive C pools. Defaults to 5. Unused if an init value is specified for any of the C pools.
        sand (np.ndarray, optional): Fraction of 0-30cm soil mass that is sand (0.050 - 2mm particles), dimensionless. Defaults to a([0.33]).
        lc (np.ndarray, optional): Lignin content of carbon input, proportion (see Table 5.5b and 5.5c for default values, otherwise compile country-specific values). Defaults to a([0.073]) which is the generic value from Table 5.5B.
        nc (np.ndarray, optional): Nitrogen fraction of the carbon input, proportion (see Tables 5.5b and 5.5c for default values, otherwise compile country-specific values). Defaults to a([0.0083]) which is the generic value from Table 5.5B.
        till_fac (np.ndarray, optional): Tillage disturbance modifier on decay rate for active and slow sub-pools, dimensionless (see Table 5.5a). Defaults to a([3.036]) which is the value for full till from Table 5.5A.
        f_1 (np.ndarray, optional): Fraction of metabolic dead organic matter decay products transferred to the active sub-pool, proportion (see Table 5.5a). Defaults to a([0.378]) which is the value from Table 5.5A.
        f_2 (np.ndarray, optional): Fraction of structural dead organic matter decay products transferred to the active sub-pool, proportion (see Table 5.5a). Defaults to a([0.368]) which is the value from Table 5.5A.
        f_3 (np.ndarray, optional): Fraction of structural dead organic matter decay products transferred to the slow sub-pool, proportion (see Table 5.5a). Defaults to a([0.455]) which is the value from Table 5.5A.
        f_5 (np.ndarray, optional): Fraction of active sub-pool decay products transferred to the passive sub-pool, proportion (see Table 5.5a). Defaults to a([0.0855]) which is the value from Table 5.5A.
        f_6 (np.ndarray, optional): Fraction of slow sub-pool decay products transferred to the passive sub-pool, proportion (see Table 5.5a). Defaults to a([0.0504]) which is the value from Table 5.5A.
        f_7 (np.ndarray, optional): Fraction of slow sub-pool decay products transferred to the active sub-pool, proportion (see Table 5.5a). Defaults to a([0.42]) which is the value from Table 5.5A.
        f_8 (np.ndarray, optional): Fraction of passive sub-pool decay products transferred to the active sub-pool, proportion (see Table 5.5a). Defaults to a([0.45]) which is the value from Table 5.5A.
        k_fac_a (np.ndarray, optional): Decay rate constant under optimal conditions for decomposition of the active SOC sub-pool, year-1 (see Table 5.5a). Defaults to a([7.4]) which is the value from Table 5.5A.
        k_fac_s (np.ndarray, optional): Decay rate constant under optimal condition for decomposition of the slow carbon sub-pool, year-1 (see Table 5.5a). Defaults to a([0.209]) which is the value from Table 5.5A.
        k_fac_p (np.ndarray, optional): Decay rate constant under optimal conditions for decomposition of the slow carbon sub-pool, year-1 (see Table 5.5a). Defaults to a([0.00689]) which is the value from Table 5.5A.

    Returns:
        IPCCResults: IPCCResults class
    """

    # Active equations
    k_a = eq.k_a(k_fac_a=k_fac_a,t_fac=t_fac,w_fac=w_fac,sand=sand,till_fac=till_fac)
    f_4 = eq.f_4(f_5=f_5,sand=sand)
    beta = eq.beta(c_input=c_input,lc=lc,nc=nc)
    alpha = eq.alpha(beta=beta,f_1=f_1, c_input=c_input, lc=lc, f_2=f_2, f_3=f_3,f_7=f_7,f_6=f_6,f_8=f_8,f_4=f_4,f_5=f_5)
    active_y_dot = eq.active_y_dot(alpha=alpha,k_a=k_a)

    # Slow equations
    k_s = eq.k_s(k_fac_s=k_fac_s,t_fac=t_fac,w_fac=w_fac,till_fac=till_fac)
    slow_y_dot = eq.slow_y_dot(c_input=c_input,lc=lc,f_3=f_3,active_y_dot=active_y_dot,k_a=k_a,f_4=f_4,k_s=k_s)

    # Passive equations
    k_p = eq.k_p(k_fac_p=k_fac_p,t_fac=t_fac,w_fac=w_fac)
    passive_y_dot = eq.passive_y_dot(active_y_dot=active_y_dot,k_a=k_a,f_5=f_5,slow_y_dot=slow_y_dot,k_s=k_s,f_6=f_6,k_p=k_p)

    # Create empty arrays to store results
    active_res = create_res(template=t_fac)
    slow_res = create_res(template=t_fac)
    passive_res = create_res(template=t_fac)

    # The initial value of the active, slow and passive C pools for the model is set based on the average steady-state value for 5 or more years prior to the first year in the inventory time series.
    active_res[0] = active_init if active_init is not None else np.mean(active_y_dot[0:run_in_period], axis = 0)
    slow_res[0] = slow_init if slow_init is not None else np.mean(slow_y_dot[0:run_in_period], axis=0)
    passive_res[0] = passive_init if passive_init is not None else np.mean(passive_y_dot[0:run_in_period], axis = 0)

    # Calculate active, slow and passive pools for all years
    for _ in range(1, active_res.shape[0], 1):
        active_res[_] = eq.active_y(active_y_1=a(active_res[_-1]), active_y_dot=a(active_y_dot[_]), k_a = a(k_a[_]))
        slow_res[_] = eq.slow_y(slow_y_1=a(slow_res[_-1]), slow_y_dot=a(slow_y_dot[_]), k_s=a(k_s[_]))
        passive_res[_] = eq.passive_y(passive_y_1=a(passive_res[_-1]), passive_y_dot=a(passive_y_dot[_]), k_p=a(k_p[_]))
    soc_res = eq.soc_y_i(active_y_i=active_res, slow_y_i=slow_res, passive_y_i=passive_res)

    # If no specific timestamps are provided, generate a sequence
    timestamps = list(range(soc_res.shape[0])) if timestamps is None else timestamps

    return IPCCResults(timestamps=timestamps, soc=soc_res, active=active_res, slow=slow_res, passive=passive_res)


def calc_nex_t(n_excretion_rate: np.ndarray, animal_mass: np.ndarray, days: np.ndarray = a([365.25])) -> np.ndarray:
    """Calculate annual average N excretion per animal of species/category T in the country, kg N animal-1 yr-1

    Args:
        n_excretion_rate (np.ndarray): Table 10.19
        animal_mass (np.ndarray): Revised Calculation of Livestock Units for Higher Level Scheme Agreements, Table 2 (in data directory) (kg animal-1)
        days (np.ndarray, optional): Days. Defaults to a([365.25]).

    Returns:
        np.ndarray: Annual average N excretion per animal of species/category T in the country, kg N animal-1 yr-1
    """
    _: np.ndarray = n_excretion_rate * (animal_mass / 1000) * days
    return _


def calc_ef_3_s_unmanaged_manure(nex_t: np.ndarray, ef_prp_cpp_so: np.ndarray) -> np.ndarray:
    """ef_3_s if for unmanaged manure only, else use table 10.21
    This wraps an eq because it may need to be invoked as a model function if the user wants to account for unmanaged manure, otherwise they would just use Table 11.1

    Args:
        nex_t (np.ndarray): As above
        ef_prp_cpp_so (np.ndarray): Table 11.1

    Returns:
        np.ndarray: Emission factor for direct N2O emissions from unmanaged manure management, kg N2O-N (kg N)-1
    """
    return eq.n2o_n_prp(f_prp_cpp_so=nex_t, ef_prp_cpp_so=ef_prp_cpp_so)


def calc_f_am_t(
    n_t: np.ndarray,
    nex_t: np.ndarray = a([68]),
    awms_ts: np.ndarray = a([0.5]), 
    n_cdg: np.ndarray = a([0]), 
    n_beddingms_ts: np.ndarray = a([0]),
    r_n2_n20: np.ndarray = a([3]),
    ef_3_s: np.ndarray = a([0]),
    frac_gas_ms_ts: np.ndarray = a([0.07]),
    frac_leachs_ms_ts: np.ndarray = a([0])
    ) -> np.ndarray:
    """Calculate N in animal manures applied to crop T, kg N yr-1 (implementation of Equation 10.34 in Section 10.5.4, Chapter 10)

    Args:
        n_t (np.ndarray): Number of head of livestock species/category T in the country
        nex_t (np.ndarray, optional): Annual average N excretion per animal of species/category T in the country, kg N animal-1 yr-1. Defaults to a([68]). Explanation for default: "...total N excretion for dairy cattle ranged from 260–475 g N/d, while for beef cattle ranged from 101 to 114 g N/d". Default value used is the average of these values, scaled to Kg N animal-1 yr-1. Source: https://www.mdpi.com/2073-4395/12/1/15/html. See also https://uk-air.defra.gov.uk/assets/documents/reports/empire/naei/annreport/annrep99/app1_212.html 
        awms_ts (np.ndarray, optional): Fraction of total annual nitrogen excretion for each livestock species/category T that is managed in manure management system S in the country, dimensionless. Defaults to a([0.5]). Explanation for default: Assume half of total annual nitrogen excretion for each livestock species/category is managed in the manure management system.
        n_cdg (np.ndarray, optional): Amount of nitrogen from co-digestates added to biogas plants such as food wastes or purpose grown crops kg N yr-1. Defaults to a([0]). Exaplanation for default: Assume no nitrogen from co-digestates added to biogas plants.
        n_beddingms_ts (np.ndarray, optional): Amount of nitrogen from bedding (to be applied for solid storage and deep bedding MMS if known organic bedding usage), kg N animal-1 yr-1. Defaults to a([0]).
        r_n2_n20 (np.ndarray, optional): Ratio of N2 : N2O emissions. The default value of RN2(N2O) is 3 kg N2-N (kg N2O-N)-1 (see Table 10.23). Defaults to a([3]).
        ef_3_s (np.ndarray, optional): Emission factor for direct N2O emissions from manure management system S in the country, kg N2O-N (kg N)-1 in manure management system S (see Table 10.21). Defaults to a([0]).
        frac_gas_ms_ts (np.ndarray, optional): Fraction of managed manure nitrogen for livestock category T that is lost by volatilisation in the manure management system S as NH3 or NOX (see Table 10.22). Defaults to a([0.07]). Explanation for default: Value for dairy cow - daily spread, from Table 10.22.
        frac_leachs_ms_ts (np.ndarray, optional): Fraction of managed manure nitrogen for livestock category T that is lost in the manure management system S by leaching or run-off (see Table 10.22). Defaults to a([0]). Explanation for default: Value for dairy cow - daily spread, from Table 10.22.

    Returns:
        np.ndarray: N in animal manures applied to crop T, kg N yr-1
    """
    frac_n2_ms_s = eq.frac_n2_ms_s(ef_3_s=ef_3_s, r_n2_n20=r_n2_n20)
    frac_lossms_ts = eq.frac_lossms_ts(frac_gas_ms_ts=frac_gas_ms_ts, frac_leachs_ms_ts=frac_leachs_ms_ts, frac_n2_ms_s = frac_n2_ms_s, ef_3_s=ef_3_s)
    f_am_t = eq.n_mms_avb(n_t=n_t, nex_t=nex_t, awms_ts=awms_ts, n_cdg=n_cdg, frac_lossms_ts=frac_lossms_ts, n_beddingms_ts=n_beddingms_ts)
    return f_am_t
