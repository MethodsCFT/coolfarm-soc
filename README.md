# SOC models

## About

This GitHub project code is provided on an "as is" basis and the user assumes responsibility for its use.

This repository contains a Python implementation of:

* The 2019 IPCC steady state Soil Organic Carbon (SOC) estimation methodology (*Tier 2 Steady State Method for Mineral Soils, Chapter 5 Cropland, 2019 Refinement to the 2006 IPCC Guidelines for National Greenhouse Gas Inventories*). The documentation for this methodology is available online [here](https://www.ipcc-nggip.iges.or.jp/public/2019rf/index.html).
* RothC (*RothC - A model for the turnover of carbon in soil*) based on the model description and users guide (updated 2014) available online [here](https://www.rothamsted.ac.uk/rothamsted-carbon-model-rothc).

## General approach

Both models have been written to accept numpy arrays to allow efficient Monte Carlo methods. Input data should be prepared in arrays where each row represents a timestep in the model, and each column represents an iteration:

|   |   |   |
|---|---|---|
| Iteration 1 | Iteration 2 | Iteration N |

|   |
|---|
| Timestep 1 |
| Timestep 2 |
| Timestep N |

, therefore:

|   |   |   |
|---|---|---|
| Timestep 1, Iteration 1 | Timestep 1, Iteration 2 | Timestep 1, Iteration 3 |
| Timestep 2, Iteration 1 | Timestep 2, Iteration 2 | Timestep 2, Iteration 3 |
| Timestep 3, Iteration 1 | Timestep 3, Iteration 2 | Timestep 3, Iteration 3 |

The functions provided in the `builders` module provide ways of creating arrays to be used as inputs to the models. 

The tests and Jupyter Notebooks in this repo. provide examples of how to build these input data arrays and how to run the models. 

## Climate data
The `climate` module provides access to climate data through open-source provider [Open-Meteo](https://open-meteo.com/en/license).

# Installation instructions

This uses `pyenv` to manage the project's Python version, `virtualenv` for managing the virtual environment and `pip-tools` for managing dependencies based on `setup.py` and `setup.cfg`.

## Set up pyenv

For a great intro to pyenv I recommend https://realpython.com/intro-to-pyenv/#virtual-environments-and-pyenv.

If you don't have pyenv installed to manage your Python installations:

    brew install pyenv pyenv-which-ext

Set up your shell enviroment. E.g. for zsh, add the following to `~/.zshrc`:

    export PATH=$(pyenv root)/shims:$PATH
    eval "$(pyenv init -)"

Restart your terminal or run `exec "$SHELL"`.

See available Python versions:

    pyenv install —-list

Install the one(s) you want, e.g.:

    pyenv install 3.9.10

Set a global default Python version, e.g.:

    pyenv global 3.9.10

Check the version has been correctly set with `python -V`. If not, try `pyenv rehash` and try again. Also see versions with `pyenv versions` (active starred).

Set your project local Python version with `pyenv local <python_version>`, e.g.:

    pyenv local 3.9.10

This will create a `.python-version` file telling `pyenv` to override it's Python version.

Check you’re using a version of Python in the `~/.pyenv` folder by running `pyenv which python`. You should see something like `Users/your_username/.pyenv/versions/3.9.10/bin/python`.

## Set up virtual environment using virtualenv

Make sure you have `virtualenv` installed for the Python version you are using:

    pip install virtualenv

From your project root folder run:

    virtualenv env

This will create an `env` virtual environment directory in your project. Activate it:

    . env/bin/activate (or source env/bin/activate)

This project has the VS Code setting to automatically activate the virtual environment in `.vscode/settings.json`:

    "python.terminal.activateEnvironment": true

This should mean that you don't need to manually activate the virtual environment when you open the project in VS Code or start a new VS Code terminal instance. You can deactivate the virtual environment by running `deactivate`.

It is recommended that you manage your depenencies using `pip-tools`:

    pip install pip-tools

After installing `pip-tools` in your virtual environment you may need to rehash to ensure that `pip-compile` is pointed at the version in the virtual environment:

    rehash

You can now use `pip-tools` to install other dependencies, which it does based on `setup.cfg`. To install the dependencies currently listed, including extra dev and test depencenies, run:

    pip-compile --extra dev --extra test && pip install -r requirements.txt

## Adding more dependencies

Add dependencies into the correct part of `setup.cfg`. Build dependencies should be listed under `install_requires`, dev or test dependency should be listed in the `dev` and `test` sublists of `[options.extras_require]`.

Use pip-tools to create a `requirements.txt` from the dependencies listed in `setup.cfg`. If you're a developer you'll probably want the dev and test extras.

    pip-compile --extra dev --extra test

Then pip install the generated requirements.txt:

    pip install -r requirements.txt

Since you will pretty much always be running these commands together, you can chain them like this:

    pip-compile --extra dev --extra test && pip install -r requirements.txt

If you want to add a public or private git repository as a dependency, you can do so under `install_requires` or any `[options.extras_require]` like this:

    ExampleRepo @ git+ssh://git@github.com/example_org/ExampleRepo.git

## Switching off type checking

If you want to switch off type checking you can set `ignore_errors = True` in `mypy.ini`.

## Testing

This project uses pytest for testing. It has been set up to display console output from tests by using the `-s` argument in `.vscode/settings.json`:

    python.testing.pytestArgs": ["test", "-s"]

To see the output, go to the Output tab in VS Code and Select `Python Test Log` from the drop down list.

To disable displaying console output from tests, remove `-s` from the args.

TEST
